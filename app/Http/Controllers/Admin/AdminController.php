<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function show()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {
        if (Auth::attempt($request->only('email', 'password'))) {
            return redirect()->intended('/admin');
        }

        $request->session()->flash('auth', 'Combinatia user/parola nu este valida.');

        return redirect()->back();
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }
}
