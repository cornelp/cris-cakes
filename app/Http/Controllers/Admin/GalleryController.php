<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;

class GalleryController extends Controller
{
    protected function createIfNotExists($path)
    {
        if (! app('files')->isDirectory($path)) {
            app('files')->makeDirectory($path);
        }
    }

    public function index()
    {
        $images = Gallery::all();

        return view('admin.gallery', compact('images'));
    }

    public function store(Request $request)
    {
        // create name
        $url = trim(strtolower(str_replace(' ', '_', $request->name)))
        . '-' . md5(time()) . '.' . $request->file('image')->clientExtension();

        // check if images folder exists
        $this->createIfNotExists(base_path() . '/public/images/');

        // check if gallery exists
        $this->createIfNotExists(base_path() . '/public/images/gallery/');

        // save in gallery folder
        $request->file('image')
            ->move(base_path() . '/public/images/gallery/', $url);

        Gallery::create([
            'url' => $url,
            'name' => $request->name,
            'description' => $request->name
        ]);

        // redirect
        return redirect('/admin/gallery');
    }

    public function destroy(Gallery $gallery)
    {
        app('files')->delete(
            base_path() . '/public/images/gallery/' . $gallery->url
        );

        $gallery->delete();

        if (request()->wantsJson()) {
            return ['success' => true];
        }

        return redirect('/admin/gallery');
    }
}
