<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\InvalidDate;
use Carbon\Carbon;

class InvalidDateController extends Controller
{
    public function index()
    {
        $dates = InvalidDate::paginate(10);

        return view('admin.invalid-dates.index', compact('dates'));
    }

    public function store(Request $request)
    {
        InvalidDate::create([
            'start_date' => Carbon::parse($request->start_date),
            'end_date' => Carbon::parse($request->end_date),
        ]);

        return redirect('/admin/invalid-dates');
    }

    public function destroy($id)
    {
        InvalidDate::findOrFail($id)
            ->delete();

        return redirect('/admin/invalid-dates');
    }
}
