<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function index(Request $request)
    {
        $startDate = Carbon::parse($request->startDate);
        $endDate = Carbon::parse($request->endDate);

        $orders = Order::with('details')
            ->inInterval([$startDate, $endDate])
            ->get();

        $total = $orders->sum(function ($order) {
            return $order->details->sum('price');
        });

        return view('admin.report.index', compact('orders', 'total', 'startDate', 'endDate'));
    }    
}
