<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get();

        return view('admin.config.index', compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = request()->type;
        $items = $this->getModel($type)::get();

        $selectedIds = Product::with($type)
            ->findOrFail($id)
            ->{$type}
            ->pluck('id')
            ->all();

        return view('admin.config.edit', compact('items', 'selectedIds', 'type'));
    }

    protected function getModel($type)
    {
        return 'App\\' . ucfirst(strtolower(substr($type, 0, -1)));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        Product::findOrFail($id)
            ->{$request->type}()
            ->sync($request->ids);

        return ['success' => true];
    }
}
