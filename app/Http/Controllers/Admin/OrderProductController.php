<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\Category;

class OrderProductController extends Controller
{
    public function create(Order $order)
    {
        $categories = Category::get(['id', 'name']);
        $products = Product::with([
            'structures' => function ($query) {
                $query->select('id', 'name');
            }, 
            'toppers' => function ($query) {
                $query->select('id', 'name');
            }, 
            'servings' => function ($query) {
                $query->select('id', 'name');
            },
        ])
            ->actives(true)
            ->get(['id', 'name', 'category_id']);

        return view('admin.order.add-product',
            compact('products', 'order', 'categories'));
    }

    public function store(Order $order)
    {
        $attributes = request()->validate([
            'product' => 'required|numeric',
            'serving' => 'required|numeric',
            'topper' => 'required|numeric',
            'structure' => 'required|numeric',
            'quantity' => 'required|numeric',
            'color' => 'nullable|max:100',
        ]);

        $product = Product::findOrFail(request('product'));

        $attributes['price'] = $product->getPrice();

        $order->details()->create($attributes);

        return redirect('/admin/orders/' . $order->id . '/edit');
    }

    public function destroy(Order $order, OrderDetail $orderDetail)
    {
        $success = $orderDetail->delete();

        return compact('success');
    }
}
