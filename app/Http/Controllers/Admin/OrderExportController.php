<?php

namespace App\Http\Controllers\Admin;

use PDF;
use App\Order;
use App\Http\Controllers\Controller;

class OrderExportController extends Controller
{
    public function excel($id)
    {
        $relations = [
            'details.product.images', 'details.structure',
            'details.serving', 'details.topper', 'comments'
        ];

        $order = Order::with($relations)
            ->findOrFail($id);

        $pdf = PDF::loadView('admin.order.pdf-export', compact('order'));

        return $pdf->download('Comanda ' . $order->id);
    }
}
