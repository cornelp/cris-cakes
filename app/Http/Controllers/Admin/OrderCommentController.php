<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OrderComment;
use App\Order;

class OrderCommentController extends Controller
{
    public function store(Request $request)
    {
        OrderComment::create($request->all());

        return redirect()->back();
    }

    public function destroy(OrderComment $comment)
    {
        $comment->delete();

        if (request()->wantsJson()) {
            return ['success' => true];
        }

        return redirect()->route('orders.index');
    }
}
