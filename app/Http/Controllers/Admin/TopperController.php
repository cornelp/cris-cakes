<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Image;
use App\Topper;

class TopperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $toppers = Topper::with('image')->get();        

        return view('admin.topper.index', compact('toppers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.topper.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = Image::fromRequest($request);

        $topper = new Topper($request->all());
        $topper->image = $image->id;

        $topper->save();

        return redirect()->route('toppers.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topper = Topper::with('image')
            ->findOrFail($id);

        return view('admin.topper.edit', compact('topper'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Topper $topper, Request $request)
    {
        $data = $request->all();
        
        if ($request->has('image')) {
            $image = Image::fromRequest($request);

            $data['image'] = $image->id;
        }

        $topper->update($data);

        return redirect()->route('toppers.index');
    }
}
