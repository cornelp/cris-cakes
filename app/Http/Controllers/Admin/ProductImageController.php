<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Image;

class ProductImageController extends Controller
{
    public function index($productId)
    {
        $product = Product::with('images')->findOrFail($productId);

        return view('admin.product.image.index', compact('product'));
    }

    public function store($productId, Request $request)
    {
        // find product
        $product = Product::findOrFail($productId);

        $image = Image::fromRequest($request);

        // save reference
        $product->images()->attach($image->id);

        return redirect('/admin/products/' . $product->id . '/images');
    }

    public function destroy($productId, $imageId)
    {
        Product::findOrFail($productId)->images()->detach($imageId);

        Image::findOrFail($imageId)->delete();

        if (request()->wantsJson()) {
            return ['success' => true];
        }

        return redirect('/admin/products/' . $productId . '/images');
    }
}
