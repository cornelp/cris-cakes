<?php

namespace App\Http\Controllers\Admin;

use App\Serving;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servings = Serving::all();        

        return view('admin.serving.index', compact('servings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.serving.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Serving::create($request->all());

        return redirect()->route('servings.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Serving $serving)
    {
        return view('admin.serving.edit', compact('serving'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Serving $serving, Request $request)
    {
        $serving->update($request->all());

        return redirect()->route('servings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Serving $serving)
    {
        return redirect()->route('servings.index');
    }
}
