<?php

namespace App\Http\Controllers\Admin;

use App\Structure;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Image;

class StructureController extends Controller
{
    public function index()
    {
        $structures = Structure::with('image')->get();

        return view('admin.structure.index', compact('structures'));
    }    

    public function edit(Structure $structure)
    {
        return view('admin.structure.edit', compact('structure'));
    }

    public function update(Structure $structure, Request $request)
    {
        $data = $request->all();

        if ($request->has('image')) {
            $image = Image::fromRequest($request);

            $data['image'] = $image->id;
        }

        $structure->update($data);

        return redirect('/admin/structures');
    }

    public function create()
    {
        return view('admin.structure.add');
    }

    public function store(Request $request)
    {
        $image = Image::fromRequest($request);

        Structure::create([
            'name' => $request->name,
            'content' => $request->content,
            'image' => $image->id
        ]);

        return redirect('/admin/structures');
    }
}
