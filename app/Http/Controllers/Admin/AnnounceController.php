<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Announce;

class AnnounceController extends Controller
{
    public function index()
    {
        $announce = Announce::first();

        return view('admin.announce.index', compact('announce'));
    }    

    public function update(Request $request)
    {
        Announce::first()
            ->update([
                'text' => $request->text,
                'active' => $request->active == 'on' ? 1 : 0
            ]);

        return redirect('/admin/announce');
    }
}
