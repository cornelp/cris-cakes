<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    protected $total = 0;

    protected function getDates($request)
    {
        $startDate = $request->has('startDate')
            ? $request->startDate
            : session()->get('startDate', now()->subYear()->format('d.m.Y'));

        $endDate = $request->has('endDate')
            ? $request->endDate
            : session()->get('endDate', now()->format('d.m.Y'));

        session()->put('startDate', $startDate);
        session()->put('endDate', $endDate);

        return [$startDate, $endDate];
    }

    protected function getOrders($search, $processed, $startDate, $endDate)
    {
        $orders = Order::with('details')
            ->processed($processed)
            ->search(compact('search', 'startDate', 'endDate'))
            ->orderBy('delivery_date')
            ->paginate(10);

        $this->total = $orders->sum(function ($order) {
            return $order->details->sum('price');
        });

        return $orders;
    }

    public function setSearch($value = null)
    {
        $search = $value ?: session('search');

        session(['search' => $search]);

        return $search;
    }

    public function processed(Request $request)
    {
        $status = 'processed';
        [$startDate, $endDate] = $this->getDates($request);
        $orders = $this->getOrders($request->search, true, $startDate, $endDate);

        $total = $this->total;

        return view('admin.order.index', compact('orders', 'status', 'startDate', 'endDate', 'total'));
    }

    public function unprocessed(Request $request)
    {
        $status = 'unprocessed';
        [$startDate, $endDate] = $this->getDates($request);
        $orders = $this->getOrders($request->search, false, $startDate, $endDate);

        $total = $this->total;

        return view('admin.order.index', compact('orders', 'status', 'startDate', 'endDate', 'total'));
    }

    public function show($id)
    {
        $relations = [
            'details.product.images', 'details.structure',
            'details.serving', 'details.topper', 'comments'
        ];

        $order = Order::with($relations)
            ->findOrFail($id);

        return view('admin.order.detail', compact('order'));
    }

    public function update(Order $order, Request $request)
    {
        $order->processed = true;
        $order->save();

        if ($request->wantsJson()) {
            return ['success' => true];
        }

        return redirect('/admin/orders');
    }
}
