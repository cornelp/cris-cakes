<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;

class ManualOrderController extends Controller
{
    public function create()
    {
        return view('admin.manual.order');
    }

    public function store()
    {
        $attributes = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'nullable',
            'delivery_date' => 'required|date|date_format:d.m.Y',
        ]);

        $order = Order::create($attributes);

        return redirect('/admin/orders/' . $order->id . '/edit');
    }
}
