<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Structure;

class StructureController extends Controller
{
    public function show(Structure $structure)
    {
        $structure->load('image');

        return view('product.structure', compact('structure'));
    }
}
