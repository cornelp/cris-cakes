<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Anam\Phpcart\Facades\Cart;

class BasketController extends Controller
{
    public function index()
    {
        $items = collect(Cart::items());
        $total = Cart::getTotal();

        return view('basket.index', compact('items', 'total'));
    }
}
