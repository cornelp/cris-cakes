<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Anam\Phpcart\Facades\Cart;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\CheckoutRequest;
use App\Mail\CustomerMail;
use App\Mail\AdminMail;
use App\InvalidDate;

class CheckoutController extends Controller
{
    public function index()
    {
        $invalidDate = InvalidDate::whereBetween('start_date', [now()->subMonth(), now()->addMonth()])
            ->get();

        return view('checkout.index', compact('invalidDate'));
    }

    public function store(CheckoutRequest $request)
    {
        $order = Order::create($request->all());

        Cart::items()->each(function ($item) use ($order) {
            $this->addDetail($order, $item);
        });

        Cart::clear();

        $order->load('details');

        Mail::to($order->email)
            ->send(new CustomerMail($order));

        Mail::to(env('MAIL_ADMIN'))
            ->send(new AdminMail($order));

        return view('checkout.thanks');
    }

    protected function addDetail($order, $item)
    {
        $serving = optional($item->product->servings->first(), function ($serving) {
            return $serving->id;
        });

        $topper = optional($item->product->toppers->first(), function ($topper) {
            return $topper->id;
        });

        $structure = optional($item->product->structures->first(), function ($structure) {
            return $structure->id;
        });

        $order->details()->create([
            'product' => $item->product->id,
            'color' => $item->color,
            'serving' => $serving,
            'topper' => $topper,
            'structure' => $structure,
            'quantity' => $item->quantity,
            'price' => $item->price,
            'description' => $item->description
        ]);
    }
}
