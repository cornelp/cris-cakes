<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    public function index($categorySlug)
    {
        $category = Category::bySlug($categorySlug)
            ->firstOrFail();

        $products = Product::with('toppers', 'images', 'servings')
            ->inCategory($categorySlug)
            ->get();

        return view('product.group', compact('products', 'category'));
    }

    public function show($categorySlug, $productId)
    {
        $product = Product::with('category', 'servings', 'toppers', 'structures', 'images')
            ->inCategory($categorySlug)
            ->findOrFail($productId);

        return view('product.single', compact('product'));
    }
}
