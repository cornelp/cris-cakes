<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announce;

class HomeController extends Controller
{
    public function index()
    {
        $announce = Announce::first();

        return view('welcome', compact('announce'));
    }    
}
