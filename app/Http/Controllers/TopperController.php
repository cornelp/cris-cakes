<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topper;

class TopperController extends Controller
{
    public function show(Topper $topper)
    {
        $topper->load('image');

        return view('product.topper', compact('topper'));
    }    
}
