<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Anam\Phpcart\Facades\Cart;
use App\Http\Requests\CartRequest;

class CartController extends Controller
{
    protected function getId(Request $request)
    {
        return md5(
            $request->name . $request->structure .
            $request->color . $request->serving .
            $request->topper . $request->description
        );
    }

    public function store(CartRequest $request)
    {
        $product = Product::with([
            'structures' => function ($query) use ($request) {
                $query->where('id', $request->structure);
            },
            'toppers' => function ($query) use ($request) {
                $query->where('id', $request->topper);
            },
            'servings' => function ($query) use ($request) {
                $query->where('id', $request->serving);
            },
        ])
            ->actives(true)
            ->findOrFail($request->product);

        if (! $product || $product->servings->count() < 1) {
            return ['success' => false];
        }

        Cart::add([
            'id'       => $this->getId($request),
            'name'     => $product->name,
            'quantity' => $request->quantity,
            'product' => $product,
            'color' => $request->color,
            'price'    => $product->getPrice(),
            'description' => $request->description
        ]);

        return $this->getItems();
    }

    protected function getItems()
    {
        $items = Cart::getItems();
        // $items = Cart::count();

        return compact('items');
    }

    public function show()
    {
        return $this->getItems();
    }

    public function update(Request $request)
    {
        $request->quantity > 0
            ? Cart::update($request->only('id', 'quantity'))
            : Cart::remove($request->id);

        return $this->getItems();
    }

    public function destroy()
    {
        Cart::clear();
    }
}
