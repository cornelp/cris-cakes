<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\InvalidDate;

class CheckInvalidDate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $date = Carbon::parse($request->input('delivery_date', now()->format('d.m.Y')));

        if (InvalidDate::isInvalid($date)) {
            return redirect()
                ->back()
                ->withErrors(['In data selectata nu se fac livrari.']);
        }

        return $next($request);
    }
}
