<?php

namespace App\Http\Middleware;

use Closure;
use App\Category;

class CheckCategory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $category = Category::actives(true)
            ->bySlug($request->route('categorySlug'))
            ->first();

        if (is_null($category)) {
            return redirect('/');
        }

        return $next($request);
    }
}
