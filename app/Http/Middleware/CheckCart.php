<?php

namespace App\Http\Middleware;

use Closure;
use Anam\Phpcart\Facades\Cart;

class CheckCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Cart::count()) {
            return redirect('/');
        }

        return $next($request);
    }
}
