<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    protected $fillable = [
        'name', 'content', 'image'
    ];

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function setImageAttribute($value)
    {
        $this->attributes['image_id'] = $value;
    }
}
