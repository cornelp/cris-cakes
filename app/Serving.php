<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Serving extends Model
{
    protected $fillable = [
        'name', 'price'
    ];

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }

    public function getPriceAttribute($value)
    {
        return $this->attributes['price'] / 100;
    }
}
