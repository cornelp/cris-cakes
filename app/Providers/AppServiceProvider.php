<?php

namespace App\Providers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Order;
use Carbon\Carbon;
use App\Observers\OrderObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Schema::defaultStringLength(191);
        Carbon::setLocale('ro');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // \DB::listen(function ($db) {
        //     echo '<pre>';
        //     var_dump($db->sql);
        //     var_dump($db->bindings);
        // });
    }
}
