<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Category;
use Blade;
use Anam\Phpcart\Facades\Cart;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.header', function ($view) {
            $view->with('categories', Category::actives(true)->get())
                ->with('cartCount', Cart::count());
        });

        Blade::directive('float', function ($expression) {
            return "<?php echo number_format($expression, 2); ?>";
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
