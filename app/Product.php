<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SetsAttribute;

class Product extends Model
{
    use SetsAttribute;

    protected $fillable = [
        'name', 'category', 'category_id', 
        'description', 'active'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function images()
    {
        return $this->belongsToMany(Image::class);
    }

    public function structures()
    {
        return $this->belongsToMany(Structure::class);
    }

    public function servings()
    {
        return $this->belongsToMany(Serving::class);
    }

    public function toppers()
    {
        return $this->belongsToMany(Topper::class)
            ->withPivot('picture');
    }

    public function setCategoryAttribute($value)
    {
        $this->set('category_id', $value);
    }

    public function scopeActives($query, $status)
    {
        $query->whereHas('category', function ($query) use ($status) {
            $query->where('active', $status);
        });
    }

    public function scopeInCategory($query, $category)
    {
        $query->whereHas('category', function ($query) use ($category) {
            $query->where('id', $category)
                ->orWhere('name', $category)
                ->orWhere('slug', $category);
        });
    }

    public function getPrice()
    {
        $servingPrice = $this->servings->first()->price;

        $topperPrice = optional($this->toppers->first(), function ($topper) {
            return $topper->price;
        });

        return $servingPrice + ($topperPrice ?? 0);
    }

}
