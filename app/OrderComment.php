<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderComment extends Model
{
    protected $fillable = [
        'order', 'comment'
    ];

    public function setOrderAttribute($value)
    {
        $this->attributes['order_id'] = $value;
    }
}
