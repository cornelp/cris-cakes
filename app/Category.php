<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'slug', 'active'
    ];

    public $timestamps = false;

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        $this->attributes['slug'] = strtolower(str_replace(' ', '-', $value));
    }

    public function scopeBySlug($query, $slug)
    {
        $query->where('slug', $slug);
    }

    public function scopeActives($query, $status)
    {
        $query->where('active', $status);
    }
}
