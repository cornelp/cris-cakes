<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SetsAttribute;

class OrderDetail extends Model
{
    use SetsAttribute;

    protected $fillable = [
        'product', 'serving', 'topper', 'structure',
        'color', 'price', 'quantity', 'description'
    ];

    public function setProductAttribute($value)
    {
        $this->set('product_id', $value);
    }

    public function setServingAttribute($value)
    {
        $this->set('serving_id', $value);
    }

    public function setTopperAttribute($value)
    {
        $this->set('topper_id', $value);
    }

    public function setStructureAttribute($value)
    {
        $this->set('structure_id', $value);
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }

    public function getPriceAttribute($value)
    {
        return $this->attributes['price'] / 100;
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function serving()
    {
        return $this->belongsTo(Serving::class);
    }

    public function structure()
    {
        return $this->belongsTo(Structure::class);
    }

    public function topper()
    {
        return $this->belongsTo(Topper::class);
    }
}
