<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topper extends Model
{
    protected $fillable = [
        'name', 'content', 'image', 'price'
    ];

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }

    public function getPriceAttribute()
    {
        return $this->attributes['price'] / 100;
    }

    public function setImageAttribute($value)
    {
        $this->attributes['image_id'] = $value;
    }
}
