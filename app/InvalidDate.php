<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvalidDate extends Model
{
    protected $fillable = [
        'start_date', 'end_date'
    ];

    protected $dates = [
        'start_date', 'end_date'
    ];

    public $timestamps = false;

    public function display()
    {
        return $this->start_date->format('d.m.Y') . ' - ' . $this->end_date->format('d.m.Y');
    }

    public static function isInvalid($date)
    {
        return (bool) static::whereDate('start_date', '<=', $date)
            ->whereDate('end_date', '>=', $date)
            ->count();
    }
}
