<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'name', 'email', 'phone', 'delivery_date', 'processed'
    ];

    protected $dates = [
        'delivery_date'
    ];

    protected $casts = [
        'processed' => 'boolean'
    ];

    public function setDeliveryDateAttribute($value)
    {
        $date = Carbon::parse($value);
        $expectedDate = now()->addDays(3);

        if ($date->lt($expectedDate)) {
            $date = $expectedDate;
        }

        $this->attributes['delivery_date'] = $date;
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function comments()
    {
        return $this->hasMany(OrderComment::class)
            ->orderBy('created_at', 'DESC');
    }

    public function scopeProcessed($query, $status)
    {
        $query->where('processed', $status);
    }

    public function scopeSearch($query, $params)
    {
        if (array_key_exists('search', $params)) {
            $query->where(function ($query) use ($params) {
                $query
                    ->where('name', 'LIKE', '%' . $params['search'] . '%')
                    ->orWhere('email', 'LIKE', '%' . $params['search'] . '%')
                    ->orWhere('phone', 'LIKE', '%' . $params['search'] . '%');
            });
        }

        $query->where(function ($query) use ($params) {
            $query->whereBetween('delivery_date', [
                Carbon::parse($params['startDate'])->startOfDay(),
                Carbon::parse($params['endDate'])->endOfDay(),
            ]);
        });
    }

    public function scopeInInterval($query, $interval)
    {
        $query->whereBetween('created_at', $interval);
    }

    public function totalPrice()
    {
        return $this->details->sum('price');
    }
}
