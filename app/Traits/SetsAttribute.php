<?php

namespace App\Traits;

trait SetsAttribute
{
    public function set($attr, $value)
    {
        $this->attributes[$attr] = is_numeric($value) ? $value : null;
    }
}