<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'name', 'alt'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public static function fromRequest($request)
    {
        $name = trim(str_replace(' ', '_', strtolower($request->name))) 
            . '-' . md5(time()) . '.' .
            $request->image->getClientOriginalExtension();

        // move picture to folder
        $request->image->move(public_path() . '/images/', $name);

        return static::create([
            'name' => $name,
            'alt' => $request->input('alt', $name)
        ]);
    }
}
