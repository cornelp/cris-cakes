@extends('layouts.master')

@section('content')
<div class="tittle-sub-top pad-top-150">
    <div class="container">
        <a href="{{ url('/') }}">Acasa</a> /
        <h1> Termeni si conditii </h1>
    </div>
</div>
</header>
</section>
<!-- End Header Cake -->
<!-- Start Terms Of Use -->
<section class="terms-of-use">
    <div class="container">
        <div class="content-terms">
            <div class="row">
                <div class="col-sm-12">
                    <div class="mainContent">
                        <section class="textPage">
                            <p>Aceasta politica de confidentialitate reglementeaza modul in care www.criscakes.ro colecteaza,
                                utilizeaza, mentine si prezinta informatii colectate de la utilizatorii (fiecare, un “utilizator”)
                                website-ului ("Site"). Aceasta politica de confidentialitate se aplica pentru Site si pentru
                                toate produsele si serviciile oferite de www.criscakes.ro</p>
                            <p>Daca aveti intrebari sau observatii cu privire la politica noastra de confidentialitate, va rugam
                                sa ne contactati pe:&nbsp;<a href="www.criscakes.ro">www.criscakes.ro</a>&nbsp;sau pe&nbsp;
                                <a
                                    href="mailto:comenzi@criscakes.ro">comenzi@criscakes.ro</a>
                            </p>
                            <p><strong>Produsele si serviciile noastre</strong></p>
                            <ul>
                                <li>Toate produsele au un termen de valabilitate de 72 de ore.</li>
                                <li>Exista posibilitatea de a fi mici modificari in ceea ce priveste formele datorita faptului
                                    ca toate torturile noastre sunt realizate manual. Pentru decorarea torturilor se folosesc
                                    coloranti solubili in apa de origine vegetala ceea ce presupune anumite diferente in
                                    nuantele de colori ale produselor.&nbsp;</li>
                                <li>Torturile pot fi livrate la orice adresa&nbsp;<b><i>din Bacau</i></b>&nbsp;. In cazul in
                                    care comanda este de peste 180 RON, livrarea este gratuita.&nbsp;Termenul de livrare
                                    este de minimum 2 zile dupa plasarea comenzii.</li>
                                <li>De asemenea, poti ridica comanda direct din Atelierul CrisCakes din Bacau, Serbanesti, str.
                                    Spicului, nr. 1 E </li>
                                <li><span class="apple-converted-space">Solicitarile de modificare a comenzii se vor onora doar daca sunt anuntate cu cel putin 3 zile inainte de data livrarii / ridicarii produsului.</span></li>
                                <li><span class="apple-converted-space">Modalitatile de plata sunt: Ramburs</span></li>
                            </ul>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Terms Of Use -->
@endsection