@extends('layouts.master')

@section('content')
<div class="tittle-sub-top pad-top-150">
    <div class="container">
        <h1>
            Confirmare comanda
        </h1>
    </div>
</div>
</header>
<div class="orange-arrow">
    &nbsp;
</div>
</section>
<!-- End Header Cake -->
<section class="404">
    <div class="container">
        <div class="content-404">
            <div class="page-header text-center">
                <h2 class="text-center">
                    Va multumim pentru comanda.<br> In cel mai scurt timp va vom contacta pentru a confirma comanda dumneavoastra.<br>                    Multumim!
                </h2>
                <a class="btn btn-orange-cake mar-right-10" href="{{ url('/') }}">Acasa</a>
            </div>
        </div>
    </div>
</section>
<!-- End 404 Page -->
<div class="pad-top-150"></div>
<!-- Start Footer Cake -->
@endsection