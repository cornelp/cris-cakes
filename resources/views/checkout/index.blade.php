@extends('layouts.master')

@section('content')
<div class="tittle-sub-top pad-top-150">
    <div class="container">
        <h1>
            Finalizare comanda
        </h1>
    </div>
</div>
</header>
<div class="orange-arrow">
    &nbsp;
</div>
</section>
<!-- End Header Cake -->
<section class="404">
    <div class="container">
        <div class="content-404">
            <div class="page-header text-center">
                <h3 class="text-center">
                    Inca un pas si comanda dumneavoastra va fi trimisa!<br> Mai avem nevoie doar de cateva detalii!
                </h3>

                @if ($invalidDate->count())
                    <div class="alert alert-danger">
                        <p>Atentie! In perioada {{ $invalidDate->first()->display() }}</p>
                        <p>NU se vor procesa comenzi! Va rugam sa aveti in vedere acest aspect.</p>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li><h3>{{ $error }}</h3></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="col-md-12">
                    <form class="form-horizontal" role="form" action="{{ url('/checkout') }}" method="POST">
                        @csrf

                        <h2>Detalii de contact</h2>

                        <div class="form-group">
                            <label for="Name" class="col-sm-3 control-label">Nume si Prenume</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ old('name') }}" name="name" id="Name" placeholder="Nume si prenume" class="form-control" autofocus required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" id="email" name="email" placeholder="Email" value="{{ old('email') }}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="telefon" class="col-sm-3 control-label">Telefon</label>
                            <div class="col-sm-9">
                                <input type="phone" name="phone" id="telefon" value="{{ old('phone') }}" placeholder="Telefon" class="form-control" autofocus required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="data-estimare" class="col-sm-3 control-label">Data la care doriti sa fie gata comanda</label>
                            <div class="col-sm-9">
                                <input type="phone" name="delivery_date" id="data-estimare" value="{{ old('delivery_date') }}" placeholder="Data" class="form-control datepicker" autofocus required>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <div class="checkbox">
                                    <label>
										<input type="checkbox" name="accept" required>Sunt de acord cu <a target="_blank" href="{{ url('/terms') }}">termenii si conditiile.</a>
									</label>
                                </div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-pink-cake">Trimite comanda</button>
                            </div>
                        </div>
                    </form>
                    <!-- /form -->



                    <p class="mar-top-0 mar-btm-20">
                        Dupa confirmarea comenzii prin telefon, produsele se vor ridica din laboratorul nostru: <b>Bacau, Serbanesti, str. Spicului, nr. 1 E </b><br>                        Vezi pe <a href="https://www.google.com/maps/place/Strada+Spicului+1,+Bacău/@46.5960868,26.9297525,17z/data=!3m1!4b1!4m5!3m4!1s0x40b56553a9a3b4ed:0x5d8ba59e4f40b8c9!8m2!3d46.5960831!4d26.9319412?hl=ro"
                            target="_blank">harta</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End 404 Page -->
<div class="pad-top-150"></div>
<!-- Start Footer Cake -->
@endsection

@section('js')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(function () {
            $('.datepicker').datepicker({
                format: 'dd.mm.yyyy',
                startDate: '{{ now()->addDays(3)->format('d.m.Y') }}'
            });
        });
    </script>
@endsection
