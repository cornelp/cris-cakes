@extends('layouts.admin.admin')

@section('content')
<div class="vd_content-wrapper" style="min-height: 1059px;">
    <div class="vd_container" style="min-height: 1059px;">
        <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
                <div class="vd_panel-header">
                    <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code."
                        data-step="5" data-position="left">
                        <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom"
                            class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                        <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom"
                            class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                        <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom"
                            class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
                    </div>
                </div>
            </div>
            <div class="vd_title-section clearfix">
                <div class="vd_panel-header">
                    <h1>Galerie Foto</h1>
                </div>
            </div>
            <div class="vd_content-section clearfix">

                <form action="{{ url('/admin/gallery') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <label>Nume</label>
                    <input type="text" name="name" class="form-control">

                    <label>Descriere</label>
                    <textarea class="form-control" rows="3" name="description"></textarea>

                    <input type="file" name="image">

                    <button class="btn btn-success">Adauga Poza</button>
                </form>

                <div class="isotope js-isotope vd_gallery" style="position: relative; height: 1600px;">
                    @if (isset($images) && count($images))
                        @foreach ($images as $image)
                            <div class="col-md-3">
                                <div class="gallery-item">
                                    <a href="{{ url('images/gallery/' . $image->url) }}">
                                        <img alt="example image" src="{{ asset('images/gallery/' . $image->url) }}">
                                        <div class="bg-cover"></div>
                                    </a>
                                    <div class="vd_info">
                                        <h3><span class="font-semibold"></span></h3>
                                        <a class="vd_bg-green vd_white mgr-10 btn btn-xs" role="button" data-id="{{ $image->id }}" onclick="deleteImage(event, this)">Sterge poza</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
                <div class="clearfix"></div>
                <br>
                <br>
            </div>
            <!-- .vd_content-section -->
        </div>
        <!-- .vd_content -->
    </div>
    <!-- .vd_container -->
</div>
@endsection

@section('js')
   <script>
        function deleteImage (event, element) {
            if (! confirm('Esti sigur(a) ca vrei sa stergi poza?')) {
                return false;
            }

            var id = element.getAttribute('data-id');

            axios.delete('/admin/gallery/' + id)
                .then(function () { $(element).parent().parent().parent().remove(); });

            event.preventDefault();
        }
    </script> 
@endsection