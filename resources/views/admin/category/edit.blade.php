@extends('layouts.admin.admin')

@section('content')
<div class="vd_content-wrapper" style="min-height: 1059px;">
   <div class="vd_container" style="min-height: 1059px;">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step="5" data-position="left">
                  <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
               </div>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Editeaza Categorie {{ $category->name }}</h1>
            </div>
         </div>
		 
		 
		 <div class="col-md-12">
			<form class="form-horizontal" role="form" action="{{ url('/admin/categories/' . $category->id) }}" method="POST">
                @csrf
                @method('PUT')

				<div class="form-group"><br><br>
					<label for="Topper" class="col-sm-3 control-label">Nume<br> <i style="font-weight:300;"></i></label>
					<div class="col-sm-9">
						<input class="form-control" name="name" type="text" value="{{ $category->name }}" id="example-text-input">
					</div>
				</div>
				
				<div class="form-group"><br><br>
					<label for="Topper" class="col-sm-3 control-label">Activa<br> <i style="font-weight:300;"></i></label>
					<div class="col-sm-9">
						<select name="active" id="">
                            <option value="0">Nu</option>
                            <option {{ $category->active ? 'selected' : '' }} value="1">Da</option>
                        </select>
					</div>
                </div>

				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-success">Salveaza</button>
                        <a href="{{ url('/admin/categories') }}" class="btn btn-info">Inapoi</a>
					</div>
				</div>
			</form> <!-- /form -->
		</div>
		 
      </div>
      <!-- .vd_content-section --> 
   </div>
   <!-- .vd_content --> 
</div>
<!-- .vd_container --> 
</div>

@endsection