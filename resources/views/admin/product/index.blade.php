@extends('layouts.admin.admin')

@section('content')
<div class="vd_container" style="min-height: 1059px;">
   <div class="vd_content clearfix">
      <div class="vd_head-section clearfix">
         <div class="vd_panel-header">
            <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step="5" data-position="left">
               <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
               <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
               <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
            </div>
         </div>
      </div>
      <div class="vd_content-section clearfix">
         <div class="row">
            <div class="col-md-12">
               <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                     <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Lista Produse</h3>
                  </div>
                  <div class="panel-body table-responsive">
                     <div id="data-tables_wrapper" class="dataTables_wrapper form-inline no-footer">
                         @if ($products->count())
                            <table class="table table-striped dataTable no-footer" id="data-tables" role="grid" aria-describedby="data-tables_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending" style="width: 268px;">#ID</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 386px;">Nume Produs</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 353px;">Categorie</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 353px;">Actiuni</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr class="gradeA odd" role="row">
                                        <td class="sorting_1">{{ $product->id }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->category->name }}</td>
                                        <td class="center">
                                            <a class="btn btn-success" type="button" href="{{ url('/admin/products/' . $product->id . '/edit') }}">Editeaza</a>
                                            <a class="btn btn-info" type="button" href="{{ url('/admin/products/' . $product->id . '/images') }}">Adauga Poze</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            </table>
                         @endif
                     </div>
                  </div>
               </div>
               <!-- Panel Widget --> 
            </div>
            <!-- col-md-12 --> 
         </div>
         <!-- row --> 
      </div>
      <!-- .vd_content-section --> 
   </div>
   <!-- .vd_content --> 
</div>

@endsection