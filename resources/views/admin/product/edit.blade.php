@extends('layouts.admin.admin')

@section('content')
    <div class="vd_content-wrapper" style="min-height: 1059px;">
   <div class="vd_container" style="min-height: 1059px;">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step="5" data-position="left">
                  <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
               </div>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Editeaza Produs</h1>
            </div>
         </div>
		 
		 
		 <div class="col-md-12">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/products/' . $product->id) }}">
                @csrf
				@method('PUT')

				<div class="form-group"><br><br>
					<label for="Topper" class="col-sm-3 control-label">Nume Produs <br> <i style="font-weight:300;"></i></label>
					<div class="col-sm-9">
						<input class="form-control" type="text" value="{{ $product->name }}" name="name" id="example-text-input">
					</div>
				</div>
				
				<div class="form-group">
					<label for="descriere" class="col-sm-3 control-label">Descriere Produs </label>
					<div class="col-sm-9">
						<textarea class="form-control" id="descriere" name="description" rows="3" placeholder="Descriere">{{ $product->description }}</textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label for="compozitie" class="col-sm-3 control-label">Categorie Produs</label>
					<div class="col-sm-9">
						<select id="category" name="category" class="form-control">
                            @foreach ($categories as $item)
                                <option {{ $item->id == $product->category->id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="compozitie" class="col-sm-3 control-label">E Activ</label>
					<div class="col-sm-9">
						<select id="category" name="active" class="form-control">
							<option {{ $product->active == 1 ? 'selected' : '' }} value="1">Da</option>
							<option {{ $product->active == 0 ? 'selected' : '' }} value="0">Nu</option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-success">Update Produs </button>
						<a type="submit" class="btn btn-info" href="{{ url('/admin/products') }}">Inapoi</a>
					</div>
				</div>
			</form> <!-- /form -->
		</div>
		 
      </div>
      <!-- .vd_content-section --> 
   </div>
   <!-- .vd_content --> 
</div>
<!-- .vd_container --> 
</div>

@endsection