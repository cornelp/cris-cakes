@extends('layouts.admin.admin')

@section('content')
<div class="vd_container" style="min-height: 1059px;">
        <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
                <div class="vd_panel-header">
                    <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code."
                        data-step="5" data-position="left">
                        <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom"
                            class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                        <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom"
                            class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                        <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom"
                            class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
                    </div>
                </div>
            </div>
            <div class="vd_content-section clearfix">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel widget">
                            <div class="panel-heading vd_bg-grey">
                                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Lista comenzi</h3>
                            </div>
                            <div class="panel-body table-responsive">
                                <div id="data-tables_wrapper" class="dataTables_wrapper form-inline no-footer">

                                    <label for="">De la:</label>
                                    <input onchange="startSearch()" type="text" id="startDate" value="{{ $startDate->format('d.m.Y') }}" class="form-control datepicker">
                                    <label for="">Pana la:</label>
                                    <input onchange="startSearch()" type="text" id="endDate" value="{{ $endDate->format('d.m.Y') }}" class="form-control datepicker">

                                    @if ($orders->count())
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nume</th>
                                                    <th>Email</th>
                                                    <th>Telefon</th>
                                                    <th>Status</th>
                                                    <th>Data creare</th>
                                                    <th>Pret total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($orders as $order)
                                                    <tr>
                                                        <td>{{ $order->id }}</td>
                                                        <td>{{ $order->name }}</td>
                                                        <td>{{ $order->email }}</td>
                                                        <td>{{ $order->phone }}</td>
                                                        <td>
                                                                {{ $order->processed ? 'Procesata' : 'Neprocesata' }}
                                                        </td>
                                                        <td>{{ $order->created_at->format('d.m.Y') }}</td>
                                                        <td>@float($order->totalPrice())</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="6"><span class="pull-right">Total:</span></td>
                                                    <td>@float($total)</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('js')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        var startDate;
        var endDate;


        var startSearch = function () {
            startDate = $('#startDate').val();
            endDate = $('#endDate').val();

            window.location.href = '/admin/reports?startDate=' + startDate + '&endDate=' + endDate;
        };

        $(function () {
            $('.datepicker').datepicker({
                format: 'dd.mm.yyyy'
            });
        });
    </script>
@endsection