@extends('layouts.admin.admin')

@section('content')

<div class="vd_content-wrapper" style="min-height: 1059px;">
   <div class="vd_container" style="min-height: 1059px;">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step="5" data-position="left">
                  <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
               </div>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Adauga o comanda noua</h1>
            </div>
         </div>

		 <div class="col-md-12">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/manual-orders') }}">
                @csrf

				<div class="form-group"><br><br>
					<label for="name" class="col-sm-2 control-label">Nume<br> <i style="font-weight:300;"></i></label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="name" value="{{ old('name') }}" id="name">
					</div>
                </div>

				<div class="form-group"><br><br>
					<label for="email" class="col-sm-2 control-label">Email<br> <i style="font-weight:300;"></i></label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="email" value="{{ old('email', 'nu-am@mail.com') }}" id="email">
					</div>
				</div>

				<div class="form-group"><br><br>
					<label for="phone" class="col-sm-2 control-label">Telefon<br> <i style="font-weight:300;"></i></label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="phone" value="{{ old('phone') }}" id="phone">
					</div>
				</div>

				<div class="form-group"><br><br>
					<label for="delivery_date" class="col-sm-2 control-label">Data de livrare<br> <i style="font-weight:300;"></i></label>
					<div class="col-sm-10">
						<input class="form-control datepicker" type="text" name="delivery_date" value="{{ old('delivery_date', now()->addDay()->format('d.m.Y')) }}" id="delivery_date">
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-2">
						<button type="submit" class="btn btn-success">Adauga comanda</button>
					</div>
				</div>
			</form> <!-- /form -->
		</div>

      </div>
      <!-- .vd_content-section -->
   </div>
   <!-- .vd_content -->
<!-- .vd_container -->

@endsection

@section('js')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

    <script>
        $(function () {
          $('.datepicker').datepicker({
            format: 'dd.mm.yyyy',
          });
        })
    </script>
@endsection