@extends('layouts.admin.admin')

@section('content')
 <div class="vd_content-wrapper" style="min-height: 1059px;">
   <div class="vd_container" style="min-height: 1059px;">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step="5" data-position="left">
                  <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
               </div>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Adauga Compozitie Noua</h1>
            </div>
         </div>
		 
		 <div class="col-md-12">
            <form enctype="multipart/form-data" class="form-horizontal" role="form" method="POST" action="{{ url('/admin/structures') }}">
                @csrf

				<div class="form-group"><br><br>
					<label for="numepoza" class="col-sm-3 control-label">Nume Compozitie <br> <i style="font-weight:300;"></i></label>
					<div class="col-sm-9">
						<input class="form-control" type="text" id="name" name="name" placeholder="Nume">
					</div>
				</div>
				
				<div class="form-group">
					<label for="descriere" class="col-sm-3 control-label">Continut </label>
					<div class="col-sm-9">
						<textarea class="form-control" id="descriere" name="content" rows="3" placeholder="Descriere"></textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label for="exemplupoza" class="col-sm-3 control-label">Incarca poza Asociata<br> <i style="font-weight:300;"></i></label>
					<div class="col-sm-9">
						<input type="file" class="custom-file-input" name="image" id="exemplupoza">
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-success">Adauga Compozitie</button>
					</div>
				</div>
			</form> <!-- /form -->
		</div>
		 
      </div>
      <!-- .vd_content-section --> 
   </div>
   <!-- .vd_content --> 
</div>
<!-- .vd_container --> 
</div>
   
@endsection