<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="{{ ltrim(public_path('css/admin/bootstrap.min.css')) }}">
	<title>Comanda</title>
	<style>
	.invoice-title h2, .invoice-title h3 {
		display: inline-block;
	}

	.table > tbody > tr > .no-line {
		border-top: none;
	}

	.table > thead > tr > .no-line {
		border-bottom: none;
	}

	.table > tbody > tr > .thick-line {
		border-top: 2px solid;
	}
	</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
    		<div class="invoice-title">
    			<h2>Formular comanda</h2><h3 class="pull-right">Nr #{{ $order->id }}</h3>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Nume</strong> {{ $order->name }}<br>
    					<strong>Email</strong> {{ $order->email }}<br>
						<strong>Telefon:</strong><br> {{ $order->phone }}<br>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right pull-right">
    				<address>
						<strong>Livrarea la:</strong><br>
                        {{ $order->delivery_date->format('d.m.Y') }}<br>
    					<strong>Data comenzii:</strong><br>
                        {{ $order->created_at->format('d.m.Y') }}<br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-sm-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Detalii comanda</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Nume produs</strong></td>
        							<td class="text-center"><strong>Compozitie</strong></td>
        							<td class="text-center"><strong>Culoare</strong></td>
        							<td class="text-center">Numar portii</td>
        							<td class="text-center">Topper</td>
        							<td class="text-center">Cantitate</td>
                                </tr>
    						</thead>
    						<tbody>
                                @foreach ($order->details as $detail)
									<tr>
										<td>{{ $detail->product->name }}</td>
										<td class="text-center">{{ $detail->structure->name }}</td>
										<td class="text-center">{{ $detail->color ?? '-' }}</td>
										<td class="text-center">{{ $detail->serving ? $detail->serving->name : '' }}</td>
										<td class="text-center">{{ $detail->topper ? $detail->topper->name : '' }}</td>
										<td class="text-center">{{ $detail->quantity }}</td>
									</tr>
								@endforeach
    							<tr>
    								<td class=""></td>
    								<td class=""></td>
    								<td class=""></td>
    								<td class=""></td>
    								<td class="text-center"><strong>Total</strong></td>
    								<td class="text-right">{{ number_format($order->details->sum('price'), 2) }}</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
</body>
</html>