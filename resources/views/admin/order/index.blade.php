@extends('layouts.admin.admin')

@section('content')
    <div class="vd_container" style="min-height: 1059px;">
   <div class="vd_content clearfix">
      <div class="vd_head-section clearfix">
         <div class="vd_panel-header">
            <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step="5" data-position="left">
               <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
               <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
               <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
            </div>
         </div>
      </div>
      <div class="vd_content-section clearfix">
         <div class="row">
            <div class="col-md-12">
               <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                     <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Lista comenzi</h3>
                  </div>
                  <div class="panel-body table-responsive">
                     <div id="data-tables_wrapper" class="dataTables_wrapper form-inline no-footer">

                            <div class="row">
                            <div class="col-xs-6">
                                <div id="data-tables_filter" class="dataTables_filter" style="display: flex;">
                                    <label>Cauta:</label>
                                    <input type="search" value="{{ app('request')->get('search') }}" id="search-input" class="form-control input-sm" aria-controls="data-tables">
                                    <input type="search" value="{{ $startDate }}" class="form-control input-sm datepicker" id="startDate" aria-controls="data-tables">
                                    <input type="search" value="{{ $endDate }}" class="form-control input-sm datepicker" id="endDate" aria-controls="data-tables">
                                    <button onclick="startSearch()" class="btn btn-success" type="button">OK</button>
                                </div>
                            </div>
                            </div>
                            <div>Total: @float($total) RON</div>
                         @if ($orders->count())
                            <table class="table table-striped dataTable no-footer" id="data-tables" role="grid" aria-describedby="data-tables_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending" style="width: 268px;">#Nr</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 386px;">Nume</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 353px;">Email</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 353px;">Telefon</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 228px;">Data comenzii</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 166px;">Data livrarii</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 166px;">Valoare comanda</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 166px;">Status</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 166px;">Actiuni</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $order)
                                    <tr class="gradeA odd" role="row">
                                        <td class="sorting_1">{{ $order->id }}</td>
                                        <td>{{ $order->name }}</td>
                                        <td>{{ $order->email }}</td>
                                        <td>{{ $order->phone }}</td>
                                        <td class="center">{{ $order->created_at->format('d.m.Y') }}</td>
                                        <td class="center">{{ $order->delivery_date->format('d.m.Y') }}</td>
                                        <td class="center">@float($order->details->sum('price')) ron</td>
                                        <td class="center">{{ $order->processed ? 'Procesata' : 'Neprocesata' }}</td>
                                            <td class="center">
                                                <div class="btn-group">
                                                <button type="button" class="btn vd_btn vd_bg-green dropdown-toggle" data-toggle="dropdown"> Actiuni <i class="fa fa-caret-down prepend-icon"></i> </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{ url('/admin/orders/' . $order->id . '/edit') }}">Vezi detalii comanda</a></li>
                                                    @if (! $order->processed)
                                                      <li><a href="#" onclick="markAsProcessed(event, '{{ $order->id }}', this)">Marcheaza ca efectuat</a></li>
                                                    @endif
                                                </ul>
                                                </div>
                                            </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            </table>

                            {{ $orders->links() }}
                        </div>
                    </div>
                         @endif
               </div>
               <!-- Panel Widget -->
            </div>
            <!-- col-md-12 -->
         </div>
         <!-- row -->
      </div>
      <!-- .vd_content-section -->
   </div>
   <!-- .vd_content -->
</div>

@endsection

@section('js')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        var markAsProcessed = function (event, id, element) {

            axios.put('/admin/orders/' + id)
                .then(function () {
                    $(element).closest('tr').remove();
                });

            event.preventDefault();
        };

        var startSearch = function () {
            var input = $('#search-input').val();
            var startDate = $('#startDate').val();
            var endDate = $('#endDate').val();

            window.location.href = '/admin/orders-{{ $status }}?search=' + input +
              '&startDate=' + startDate + '&endDate=' + endDate;
        };

        $(function () {
          $('.datepicker').datepicker({
            format: 'dd.mm.yyyy',
          });
        });
    </script>
@endsection
