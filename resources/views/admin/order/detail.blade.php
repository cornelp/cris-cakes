@extends('layouts.admin.admin')

@section('content')
<div class="vd_container">
    <div class="vd_content clearfix">
        <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
                <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step="5" data-position="left">
                    <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                    <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                    <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
                </div>
            </div>
        </div>
        
        <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
                <h1>Comanda numarul {{ $order->id }} facuta la data de {{ $order->created_at->format('d.m.Y') }}</h1>
            </div>
        </div>
        <div class="vd_content-section clearfix">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="menu-text">
                        <div class="menu-info">
                            <a href="{{ url('/admin/orders-' . ($order->processed ? 'processed' : 'unprocessed')) }}" class="btn btn-success">Inapoi la comenzi {{ $order->processed ? 'procesate' : 'neprocesate' }}</a>
                            
                            <div class="pull-right">
                                <a href="#" id="showProductForm" class="btn btn-warning">Adauga produse la comanda</a>
                                <a href="/admin/orders/{{ $order->id }}/export" class="btn btn-success">Export PDF</a>
                            </div>
                            
                            <div class="menu-date font-xs">Pretul estimativ al comenzii: @float($order->details->sum('price')) RON</div>
                            <div class="menu-tags  mgbt-xs-15"> Detalii Contact: <a href="#"><span class="label vd_bg-yellow">{{ $order->name }}</span></a> <a href="#"><span class="label vd_bg-yellow">{{ $order->phone }}</span></a> <a href="#"><span class="label vd_bg-yellow">{{ $order->email }}</span></a> </div>
                            <p>Data la care clientul vrea produsele:  {{ $order->delivery_date->format('d.m.Y') }}</p>
                            
                            <div>
                                <h3>S-au comandat urmatoarele produse:</h3>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="panel widget">
                                    <div class="panel-heading vd_bg-grey">
                                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span> Comanda</h3>
                                    </div>
                                    <div class="panel-body table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Cantitate</th>
                                                    <th>Denumire produs</th>
                                                    <th>Compozitie</th>
                                                    <th>Culoare </th>
                                                    <th>Numar portii</th>
                                                    <th>Topper </th>
                                                    <th>Alte mentiuni</th>
                                                    <th>Poza</th>
                                                    <th>Actiuni</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($order->details as $detail)
                                                <tr>
                                                    <td>{{ $detail->quantity }}</td>
                                                    <td>{{ $detail->product->name }}</td>
                                                    <td>{{ $detail->structure ? $detail->structure->name : '-' }}</td>
                                                    <td>{{ $detail->color ?? '-' }}</td>
                                                    <td>{{ $detail->serving ? $detail->serving->name : '-' }}</td>
                                                    <td>{{ $detail->topper ? $detail->topper->name : '-' }}</td>
                                                    <td class="center">{{ $detail->description }}</td>
                                                    <td class="center"><a href="{{ asset('/images/' . $detail->product->images->first()->name) }}" target="_blank"/>Poza</a></td>
                                                    <td class="center">
                                                        <a onclick="deleteDetail(event, this, {{ $detail->id }})" href="#">Sterge</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- Panel Widget -->
                            </div>
                            
                            <div class="col-md-12">
                                <div class="panel widget">
                                    <div class="panel-heading vd_bg-grey">
                                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span> Comentarii</h3>
                                    </div>
                                    
                                    <div class="panel-body col-md-12">
                                        <form method="POST" action="{{ url('/admin/comments') }}">
                                            @csrf
                                            
                                            <input type="hidden" value="{{ $order->id }}" name="order">
                                            
                                            <div class="form-group">
                                                <label for="">Text</label>
                                                <input type="text" name="comment" class="form-control">
                                                <button class="btn btn-success">Adauga comentariu</button>
                                            </div>
                                        </form>
                                        
                                        @foreach ($order->comments as $comment)
                                        <span class="alert alert-info col-md-12 comment">
                                            [{{ $comment->created_at->diffForHumans() }}]<br>
                                            {{ $comment->comment }}
                                            <span class="pull-right">
                                                <a href="#" onclick="deleteComment(event, this, {{ $comment->id }})">Sterge</a>
                                            </span>
                                        </span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <!-- col-md-4 -->
                        
                    </div>
                    <!--row -->
                    
                </div>
                <!-- .vd_content-section -->
                
            </div>
            <!-- .vd_content -->
            <!-- .vd_container -->
            
            <div class="modal fade" id="addProducts" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Adauga produs</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        
        @endsection
        
        @section('js')
        <script>
            var deleteComment = function (event, element, id) {
                axios.delete('/admin/comments/' + id)
                    .then(function () {
                        $(element).closest('.comment').remove();
                    });
                
                event.preventDefault();
            };
            
            $('#showProductForm').on('click', function (event) {
                event.preventDefault();
                
                axios.get('/admin/orders/{{ $order->id }}/add-products/create')
                    .then(function (response) {
                        $('.modal-body').html(response.data);
                        
                        $('#addProducts').modal('show');
                    });
            });

            var deleteDetail = function (event, element, id) {
                axios.delete('/admin/orders/{{ $order->id }}/details/' + id)
                    .then(function () {
                        location.reload();
                    });

                event.preventDefault();
            };
        </script>
        @endsection
        