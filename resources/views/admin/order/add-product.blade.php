<form action="/admin/orders/{{ $order->id }}/add-products" method="POST">
    @csrf
    
    <div class="form-group">
        <label for="category" class="col-sm-2 control-label">Categorie</label>
        <div class="col-sm-10">
            <select class="form-control" name="category" id="category">
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    
    <div class="form-group">
        <label for="product" class="col-sm-2 control-label">Produs</label>
        <div class="col-sm-10">
            <select class="form-control" name="product" id="product">
                @foreach ($products as $product)
                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="serving" class="col-sm-2 control-label">Nr de portii</label>
        <div class="col-sm-10">
            <select class="form-control" name="serving" id="serving">
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="structure" class="col-sm-2 control-label">Compozitie</label>
        <div class="col-sm-10">
            <select class="form-control" name="structure" id="structure">
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="topper" class="col-sm-2 control-label">Toppers</label>
        <div class="col-sm-10">
            <select class="form-control" name="topper" id="topper">
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="color" class="col-sm-2 control-label">Culoare</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="color" id="color">
        </div>
    </div>

    <div class="form-group">
        <label for="quantity" class="col-sm-2 control-label">Cantitate</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="quantity" id="quantity" value="1">
        </div>
    </div>

    <button class="btn btn-success" type="submit">Adauga produs</button>
</form>

<script>
    $(function () {
        var categories = '{!! $categories !!}';
        var products = JSON.parse('{!! $products->toJson() !!}');
        
        $('#category').on('change', function () {
            var categoryId = $(this).val();
            
            var options = products.filter(function (item) {
                return item.category_id == categoryId;
            });
            
            setOptions('#product', options);

            $('#product').trigger('change');
        });

        $('#product').on('change', function () {
            var productId = $(this).val();

            var selected = products.find(function (product) {
                return product.id == productId;
            });

            setOptions('#serving', selected.servings);
            setOptions('#topper', selected.toppers);
            setOptions('#structure', selected.structures);
        });

        var setOptions = function (selector, options) {
            var options = options.map(function (object) {
                return '<option value="' + object.id + '">' + object.name + '</option>';
            });

            $(selector).html(options);
        };
        
        $('#category').trigger('change');
        $('#product').trigger('change');
    });
</script>