@extends('layouts.admin.admin')

@section('content')
<div class="vd_container" style="min-height: 1059px;">
   <div class="vd_content clearfix">
      <div class="vd_head-section clearfix">
         <div class="vd_panel-header">
            <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step="5" data-position="left">
               <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
               <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
               <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
            </div>
         </div>
      </div>
      <div class="vd_content-section clearfix">
         <div class="row">
            <div class="col-md-12">
               <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                     <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Produs - Topper</h3>
                  </div>
                  <div class="panel-body table-responsive">
                     <div id="data-tables_wrapper" class="dataTables_wrapper form-inline no-footer">
                         @if ($products->count())
                            <table class="table table-striped dataTable no-footer" id="data-tables" role="grid" aria-describedby="data-tables_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending" style="width: 268px;">Produs</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 386px;">Seteaza Compozitiile</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 353px;">Seteaza Numarul de Portii</th>
                                    <th class="sorting" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 228px;">Seteaza Toppere</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr class="gradeA odd" role="row">
                                        <td class="sorting_1">{{ $product->name }}</td>
                                        <td><p><button class="btn btn-primary" onclick="setIdAndType({{ $product->id }}, 'structures')" data-toggle="modal" data-target="#myModal"> Compozitii </button></p></td>
                                        <td><p><button class="btn btn-primary" onclick="setIdAndType({{ $product->id }}, 'servings')" data-toggle="modal"  data-target="#myModal"> Numar Portii </button></p></td>
                                        <td><p><button class="btn btn-primary" onclick="setIdAndType({{ $product->id }}, 'toppers')" data-toggle="modal" data-target="#myModal"> Toppere </button></p></td>
                                    </tr>
                                @endforeach
                            </tbody>
                            </table>
                         @endif
                     </div>
                  </div>
               </div>
               <!-- Panel Widget --> 
            </div>
            <!-- col-md-12 --> 
         </div>
         <!-- row --> 
      </div>
      <!-- .vd_content-section --> 
   </div>
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header vd_bg-blue vd_white">
                            <input type="hidden" id="selected-id">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                          <h4 class="modal-title" id="myModalLabel">Configurare</h4>
                        </div>
                        <div class="modal-body"> 
                        
                        </div>
                        <div class="modal-footer background-login">
                          <button type="button" class="btn vd_btn vd_bg-grey" data-dismiss="modal">Anuleaza</button>
                          <button type="button" onclick="saveSelected()" class="btn vd_btn vd_bg-green">Salveaza</button>
                        </div>
                      </div>
                      <!-- /.modal-content --> 
                    </div>
                    <!-- /.modal-dialog --> 
                  </div>
    </div>
   <!-- .vd_content --> 
</div>

@endsection

@section('js')
    <script>
        var selectedId = null;
        var selectedType = null;

        var setIdAndType = function (id, type) {
            selectedId = id;
            selectedType = type;
        };

        var saveSelected = function () {
            var ids = $('input:checkbox:checked')
                .map(function () {
                    return $(this).val();
                })
                .toArray();

            axios.put('/admin/config/' + selectedId, { ids: ids, type: selectedType });
        };

        $(function () {
            var selector = $('#myModal');
            var body = selector.find('.modal-body');

            selector.on('shown.bs.modal', function (e) {
                body.html('');

                axios.get('/admin/config/' + selectedId + '/edit', { params: {type: selectedType} })
                    .then(function (response) {
                        body.html(response.data);
                    });
            });
        });
    </script>
@endsection