<table class="table table-striped dataTable no-footer" id="data-tables" role="grid" aria-describedby="data-tables_info">
    <thead>
        <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending" style="width: 268px;">Compozitie</th>
            <th class="sorting_asc" tabindex="0" aria-controls="data-tables" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending" style="width: 268px;">Activ</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($items as $item)
            <tr class="gradeA odd" role="row">
                <td class="sorting_1">{{ $item->name }}</td>
                <td>
                    <div class="form-check">
                        <input name="items[]" type="checkbox" {{ in_array($item->id, $selectedIds) ? 'checked' : '' }} class="form-check-input" value="{{ $item->id }}" id="exampleCheck1">
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>