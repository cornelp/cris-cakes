@extends('layouts.admin.admin')

@section('content')
<div class="vd_container" style="min-height: 1059px;">
        <div class="row" id="html5-editor">
            <div class="col-md-12">
                <div class="panel widget">
                    <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-pencil"></i> </span> Wysiwyg HTML 5 Editor </h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ url('/admin/announce') }}" role="form" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
    
                                    <div class="row">
                                        <div class="col-lg-12 nopadding">
                                            <textarea id="txtEditor" name="text">{{ $announce->text }}</textarea>
                                        </div>
                                    </div>  
                                    <br>
                                    <br>
                                    <br> Status anunt site: <input type="checkbox" name="active" {{ $announce->active ? 'checked' : '' }} data-toggle="toggle">
                            </div>
                            <button class="btn btn-success">Salveaza</button>
                        </form>
                    </div>
                </div>
@endsection

@section('js')
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/richtext.min.css') }}">
    <script src="{{ asset('js/richtext.min.js') }}"></script>

    <script>
        $(function () {
            $('#txtEditor').richText();
        });
    </script>
@endsection