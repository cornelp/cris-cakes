@extends('layouts.admin.admin')

@section('content')
    <div class="vd_container" style="min-height: 1059px;">
        <div class="col-md-12">
            <h2>Adauga Perioada</h2>

            <form action="{{ url('/admin/invalid-dates') }}" method="POST">
                @csrf

              <div class="form-group">
                <label for="start_date">Perioada de inceput</label>
                <input type="text" name="start_date" class="form-control datepicker" id="start_date">
            </div>
            <div class="form-group">
                <label for="end_date">Perioada de sfarsit:</label>
                <input type="text" class="form-control datepicker" name="end_date" id="end_date">
            </div>
            <button type="submit" class="btn btn-default">Salveaza</button>
        </form>

        <br>
        <h2>Perioade existente</h2>

        @if (! $dates->count())
                Nicio perioada
            @else

                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Perioada</th>
                            <th>Actiuni</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($dates as $row)
                            <tr>
                                <td>{{ $row->id }}</td>
                                <td>{{ $row->display() }}</td>
                                <td>
                                    <a href="{{ url('/admin/invalid-dates/' . $row->id . '/delete') }}">Sterge</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
        </div>
    </div>
@endsection

@section('js')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

    <script>
        $(function () {
            $('.datepicker').datepicker({
                format: 'dd.mm.yyyy'
            });
        });
    </script>
@endsection
