@extends('layouts.admin.admin')

@section('content')
<div class="vd_content-wrapper">
    <div class="vd_container">
        <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
                <div class="vd_panel-header">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('/admin') }}">Acasa</a> </li>
                        <li class="active">Admin</li>
                    </ul>
                    <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code."
                        data-step=5 data-position="left">
                        <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom"
                            class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                        <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom"
                            class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                        <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom"
                            class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
                    </div>
                </div>
            </div>
            <!-- vd_head-section -->
            <div class="vd_title-section clearfix">
                <div class="vd_panel-header">
                    <h1>Dashboard</h1>
                    <small class="subtitle">Pagina de administrare CrisCakes</small>
                    <!-- vd_panel-menu -->
                </div>
                <!-- vd_panel-header -->
                <div class="row">
                    <div class="col-md-12">
                        *Pentru a administra pozele din galerie accesati meniul <b>"Galerie"</b> <br> *Pentru a adauga si
                        vizualiza produse accesati meniul <b>Produse</b> </br>
                        *Pentru a adauga si vizualiza compozitii accesati meniul <b>Compozitii</b> <br> *Pentru a adauga si
                        vizualiza numarul de portii accesati meniu <b>Numar Portii</b> <br> *Pentru a adauga si vizualiza toppere accesati
                        meniu <b>Toppere</b> <br> *Pentru a activa un anunt informativ pentru clientii care intra pe site
                        accesati meniul <b>Anunt Site</b> si de acolo va setati ce mesaj doriti sa le transmiteti. <br> 
                        <br> ***Pentru a <b>configura produsele</b> trebuie sa aveti aveti in vedere urmatoarele: <br>
                        <ul>
                            <li>Produsul trebuie sa fie adaugat</li>
                            <li>Compozitiile trebuie setate cu nume, descriere si imagine</li>
                            <li>Numarul de portii trebuie setat cu denumire si pret</li>
                            <li>Topperele trebuie setate cu denumire, descriere, imagine si pret.</li>
                        </ul>
                        <br> *Dupa ce aveti setate toate acestea, puteti configura produsele dupa bunul plac.
                    </div>
                </div>
            </div>

        </div>
        <!-- .vd_content-section -->
    </div>
    <!-- .vd_content -->
</div>
<!-- .vd_container -->
</div>
<!-- .vd_content-wrapper -->
<!-- Middle Content End -->
</div>
<!-- .container -->
</div>
<!-- .content -->
@endsection