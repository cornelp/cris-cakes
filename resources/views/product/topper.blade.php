<div class="col-sm-12">
    <div class="multi-collapse collapse in" id="multiCollapseExample1" aria-expanded="true" style="">
        <div class="card card-body">
            <div class="col-sm-6 col-xs-12">
                <img src="{{ asset('images/' . $topper->image->name) }}" alt="{{ $topper->image->alt }}" class="img-responsive">
            </div>
            <div class="col-sm-6 col-xs-12" style="padding:10px 5px;">
                Pret: @float($topper->price)
            </div>
            <div class="col-sm-6 col-xs-12" style="padding:10px 5px;">
                {{ $topper->content }}
            </div>
        </div>
    </div>
</div>