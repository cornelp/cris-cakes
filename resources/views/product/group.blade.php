@extends('layouts.master')

@section('content')
<div class="tittle-sub-top pad-top-150">
    <div class="container">
        <a href="{{ url('/') }}">Acasa</a>
        <h1>{{ $category->name }}</h1>
    </div>
</div>
</header>
<div class="purple-arrow">
    &nbsp;
</div>
<!-- Produs -->
@foreach ($products as $product)
    <div class="chart-cake">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <a href="{{ url($category->slug . '/' . $product->id) }}">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                @foreach ($product->images as $image)
                                    <div class="item {{ $loop->first ? 'active' : '' }}">
                                        <img src="{{ asset('images/' . $image->name) }}" alt="{{ $image->alt }}" class="img-responsive" style="width:100%;">
                                    </div>
                                @endforeach
                            </div>
							<a class='left carousel-control' href='#myCarousel' data-slide='prev'>
								<span class='glyphicon glyphicon-chevron-left'></span>
							</a>
							<a class='right carousel-control' href='#myCarousel' data-slide='next'>
								<span class='glyphicon glyphicon-chevron-right'></span>
							</a>
                        </div>
                    </a>
                </div>
                <div class="col-sm-8">
                    <div class="tittle-chart-cake">
                        <a href="{{ url($category->slug . '/' . $product->id) }}">
                            <h2 class="pink-color">{{ $product->name }}</h2>
                        </a>
                    </div>
                    <div class="tittle-chart-cake mar-top-10">
                        <h3 class="pink-color">
                            De la @float($product->servings->min('price')) RON
                        </h3>
                    </div>
                    <p class="mar-top-10 mar-btm-20">
                        {{ $product->description }}
                    </p>

                    <a href="{{ url('/' . $category->slug . '/' . $product->id) }}" class="btn btn-pink-cake mar-right-10">Vezi detalii</a>
                </div>
            </div>
        </div>
    </div>
@endforeach
</section>
<div class="pad-top-150"></div>
<!-- Start Footer Cake -->
@endsection
