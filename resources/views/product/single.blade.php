@extends('layouts.master')

@section('content')
<div class="tittle-sub-top pad-top-150">
    <div class="container">
        <a href="{{ url('/') }}">Acasa</a>
        <h1>{{ $product->name }}</h1>
    </div>
</div>
</header>
<div class="purple-arrow">
    &nbsp;
</div>
<div class="chart-cake">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">

                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        @foreach ($product->images as $image)
                            <div class="cevabun item {{ $loop->first ? 'active' : '' }}">
                                <img src="{{ asset('images/' . $image->name) }}" alt="{{ $image->alt }}" class="img img-responsive full-width" style="width:100%;">
                            </div>
                        @endforeach
                    </div>
                </div>
				<a class='left carousel-control' href='#myCarousel' data-slide='prev'>
					<span class='glyphicon glyphicon-chevron-left'></span>
				</a>
				<a class='right carousel-control' href='#myCarousel' data-slide='next'>
					<span class='glyphicon glyphicon-chevron-right'></span>
				</a>

            </div>
            <div class="col-sm-8">
                <div class="shop-back">
                    <a href="{{ url($product->category->slug) }}">&lt;-- Continua Cumparaturile</a>
                </div>
                <div class="tittle-chart-cake">
                    <h1 class="pink-color">
                        {{ $product->name }}
                    </h1>
                </div>
                <div class="tittle-chart-cake mar-top-10 mar-btm-10">
                    <h2 class="pink-color">
                        Pret estimativ: <span class="info-price"></span> RON
                    </h2>
                    <p><b style="font-size:16px;color:#fba1a1;">DE RETINUT!</b> Pretul se actualizeaza in functie de preferintele
                        dumneavoastra si acesta este un pret estimativ. In momentul in care veti fi sunat pentru confirmare
                        veti afla pretul exact al produsului.</p><br>Toate pozele sunt cu titlu informativ! Va puteti comanda produsul dorit dupa preferinte!
                </div>
                <div class="row">
                    <div class="col-md-12">

                        <div class="alert alert-danger hidden">
                            <ul>
                            </ul>
                        </div>

                        <form class="form-horizontal" role="form">
                            <h2>Formular de comanda</h2>
                            <div class="form-group">
                                <label for="quantity" class="col-sm-3 control-label" style="color:#fba1a1;">Cantitate</label>
                                <div class="col-sm-9">
                                    <input type="hidden" name="product" value="{{ $product->id }}" placeholder="1" id="cantitate" class="form-control">
                                    <input type="text" name="quantity" placeholder="1" value="1" id="cantitate" class="form-control">
                                    <span class="help-block"><i style="font-size:12px;color:#fba1a1;">Alege cantitatea dorita </i></span>
                                </div>
                            </div>
                            <!-- /.form-group -->
                            @if ($product->structures->count())
                                <div class="form-group">
                                    <label for="compozitie" class="col-sm-3 control-label" style="color:#fba1a1;">Compozitie</label>
                                    <div class="col-sm-9">
                                        <select id="structure" name="structure" class="form-control">
                                            @foreach ($product->structures as $structure)
                                                <option value="{{ $structure->id }}">{{ $structure->name }}</option>
                                            @endforeach
                                        </select>

                                        <div class="structure-detail hidden"></div>

                                        <span class="help-block"><i style="font-size:12px;color:#fba1a1;">Alegeti compozitia dorita.</i></span>
                                    </div>
                                </div>

                            @endif
                            <!-- /.form-group -->
                            @if ($product->structures->count())
                                <div class="form-group">
                                    <label for="culoare" class="col-sm-3 control-label" style="color:#fba1a1;">Culoare <br> <i style="font-weight:300;">optional</i></label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="culoare" name="color" rows="1" placeholder="Alegeti culoarea dorita"></textarea>
                                        <span class="help-block"><i style="font-size:12px;color:#fba1a1;">Va precizam faptul ca exista posibilitatea sa nu iasa exact nuanta dorita. Atat dumneavoastra cat si noi trebuie sa ne asumam acest lucru. Multumim!</i></span>
                                    </div>
                                </div>
                            @endif

                            @if ($product->servings->count())
                                <div class="form-group">
                                    <label for="cantitate" class="col-sm-3 control-label" style="color:#fba1a1;">Numar portii</label>
                                    <div class="col-sm-9">
                                        <select name="serving" id="serving" class="form-control">
                                            @foreach ($product->servings as $serving)
                                                <option data-price="{{ $serving->price }}" value="{{ $serving->id }}">{{ $serving->name }} (@float($serving->price) RON)</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block"><i style="font-size:12px;color:#fba1a1;"><b>Atentie!</b>Pretul se actualizeaza in functie de optiunea aleasa</i></span>
                                    </div>
                                </div>
                            @endif

                            <!-- /.form-group -->

                            @if ($product->toppers->count())
                                <div class="form-group">
                                    <label for="topper" class="col-sm-3 control-label" style="color:#fba1a1;">Topper <br> <i style="font-weight:300;">optional</i></label>
                                    <div class="col-sm-9">
                                        <select id="topper" name="topper" class="form-control">
                                            <option data-price="0">Alege</option>
                                            @foreach ($product->toppers as $topper)
                                            <option data-price="{{ $topper->price }}" value="{{ $topper->id }}">{{ $topper->name }} (+ @float($topper->price) RON)</option>
                                            @endforeach
                                        </select>

                                        <div class="topper-detail hidden"></div>

                                        <span class="help-block"><i style="font-size:12px;color:#fba1a1;">Daca doriti topper pentru tort va rugam sa va alegeti topper-ul dorit. Acestea sunt contra cost in functie de model. Va rugam sa precizati la campul mentiuni cifra, numele, modelul, etc.</i></span>
                                    </div>
                                </div>
                            @endif
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label for="mentiuni" class="col-sm-3 control-label" style="color:#fba1a1;">Alte mentiuni </label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="mentiuni" name="description" rows="3" placeholder="Daca nu aveti alte preferinte si doriti ca noi sa va surprindem cu decorul nostru, va rugam sa ne specificati aici. Multumim!"></textarea>
                                    <span class="help-block"><i style="font-size:12px;color:#fba1a1;">Daca aveti alte preferinte va rog sa le scrieti aici.</i></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" class="add-to-cart btn btn-pink-cake">Adauga in cos</button>
                                </div>
                            </div>

                            @csrf

                        </form>
                        <!-- /form -->

                        <p class="mar-top-0 mar-btm-20">
                            Info:<br> Dupa confirmarea comenzii prin telefon, produsele se vor ridica din laboratorul nostru:<br>
                            <b>Bacau, Serbanesti, str. Spicului, nr. 1 E </b><br> Vezi pe <a href="https://www.google.com/maps/place/Strada+Spicului+1,+Bacău/@46.5960868,26.9297525,17z/data=!3m1!4b1!4m5!3m4!1s0x40b56553a9a3b4ed:0x5d8ba59e4f40b8c9!8m2!3d46.5960831!4d26.9319412?hl=ro"
                                target="_blank">harta</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
@endsection

@section('js')
    <script>
        var show = function (type) {
            var typeVal = $('#' + type).val();

            if (isNaN(typeVal)) {
                return $('.' + type + '-detail').html('');
            }

            $.get('/' + type + '/' + typeVal, function (data) {
                $('.' + type + '-detail')
                    .removeClass('hidden')
                    .html(data);
            });
        }

        var setServingPrice = function (selector) {
            var price = parseFloat($('#serving option:selected').attr('data-price')).toFixed(2);

            $(selector).html(price);

            return price;
        };

        var updatePrice = function (selector, price) {
            var target = $(selector);

            // ensure we get the right price
            var basePrice = setServingPrice(selector);

            // add topper price
            target.html((parseFloat(basePrice) + parseFloat(price)).toFixed(2));
        }

        $(function () {
            show('structure');
            setServingPrice('.info-price');

            $('#structure').on('change', function (event) {
                show('structure');
            });

            $('#serving').on('change', function (event) {
                setServingPrice('.info-price');
            });

            $('.add-to-cart').on('click', function (event) {
                event.preventDefault();

                addToCart($(this).closest('form'));
            });

            $('#topper').on('change', function (event) {
                show('topper');

                updatePrice(
                    '.info-price',
                    parseFloat($(this).find('option:selected').attr('data-price'))
                );
            });
        });
    </script>
@endsection
