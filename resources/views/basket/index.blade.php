@extends('layouts.master')

@section('content')
<div class="tittle-sub-top pad-top-150">
    <div class="container">
        <a href="{{ url('/') }}">Acasa</a> /
        <h1>Cos de cumparaturi</h1>
    </div>
</div>
</header>
<div class="purple-arrow">
    &nbsp;
</div>
<div class="chart-cake">
    @if (count($items))
        <div class="container table-responsive">
            <table class="table table-bordered table-hover ">
                <thead>
                    <tr>
                        <th>Nr</th>
                        <th>Produs</th>
                        <th>Detalii</th>
                        <th>Cantitate</th>
                        <th>Pret</th>
                        <th>Actiune</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($items as $item)
                        <tr>
                            <td>1</td>
                            <td>{{ $item->name }}</td>
                            <td class="chart-description">
                                <p class="mar-top-10 pad-top-10 top-dashed">
                                    Numar portii: {{ $item->product->servings->first()->name ?? '-' }}
                                </p>
                                @if ($item->product->toppers->first())
                                    <p class="mar-top-10 pad-top-10 top-dashed">
                                        Topper: {{ $item->product->toppers->first()->name ?? '-' }}
                                    </p>
                                @endif
                                @if ($item->color)
                                    <p class="mar-top-10 pad-top-10 top-dashed">
                                        Culoare: {{ $item->color }}
                                    </p>
                                @endif
                                <p class="mar-top-10 pad-top-10 top-dashed">
                                    Compozitie: {{ $item->product->structures->first()->name ?? '-' }}
                                </p>
                            </td>
                            <td>
                                <input type="text" class="form-control" id="{{ $item->id }}" value="{{ $item->quantity }}">
                                <button onclick="updateCart('{{ $item->id }}', '{{ $item->id }}')" class="btn btn-pink-cake mar-right-10">Actualizeaza</button>
                            </td>
                            <td>
                                @float($item->price * $item->quantity) RON
                            </td>
                            <td class="chart-center">
                                <button onclick="deleteFromCart('{{ $item->id }}')" class="btn btn-pink-cake mar-right-10">Sterge</button>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
			<div class="alert alert-info" role="alert">
			  <h4 class="alert-heading">Bravo!</h4>
			  <p>Ne bucuram ca ai ajuns pana aici si va instiintam ca ati facut alegerea corecta!</p>
			  <hr>
			  <p class="mb-0">Valoarea cosului dumneavoastra de cumparaturi este de:<b> @float($total) RON </b></p>
			</div>
                <div class="top-cake-button text-center">
                    <a class="btn btn-pink-cake mar-right-10" href="{{ url('/checkout') }}">Finalizare comanda</a>
                </div>
        </div>
    @else
        <div class="container">
            <div class="alert alert-info">
                Cosul tau e gol.
            </div>
        </div>
    @endif
</div>
</section>
<!-- End Header Cake -->
<div class="pad-top-150"></div>
<!-- Start Footer Cake -->
@endsection

@section('js')
    <script>
        var updateCart = function (id, selector) {
            makeCall({ id: id, quantity: $('#' + selector).val() });
        };

        var deleteFromCart = function (id) {
            makeCall({ id: id, quantity: 0 });
        };

        var makeCall = function (data) {
            axios.put('/cart', data)
                .then(function (response) { location.reload(); })
                .catch(function (err) { location.reload(); });
        }
    </script>
@endsection
