        <footer>
          <div class="triangle-no-animate">
            &nbsp;
          </div>
          <div class="container">
            <div class="top-footer">
              <div class="row">
                <div class="col-sm-6">
                  <img alt="Logo-White" class="img-cake-center-res mar-btm-20" src="{{ asset('images/logo.png') }}">
                </div>
              </div>
            </div>
            <div class="line-top-white mar-btm-20 mar-top-20">
              &nbsp;
            </div>
            <div class="content-about-footer">
              <!-- Column -->
              <div class="col-sm-4">
                <h4>
                  CrisCakes
                </h4>
                <p class="mar-btm-20">
                  Cu pasiune, zambete si bucurie  <br>va invit sa gustati prajituri <br>din bucataria mea!<br>
                </p>
                <p class="mar-btm-20">
                  Serbanesti, str Spicului nr 1 E <br>Bacau, Romania<br>
                </p>
                <p class="mar-btm-20">
                  Tel: <strong> 0726 192 767</strong>
                </p>
              </div>
              <!-- Column -->
              <div class="col-sm-4 hidden-xs">
                <ul class="list-picture-footer">
                  <li>
                    <a class="fancybox" data-fancybox-group="contentgallery" href="{{ asset('images/footer (1).jpg') }}"><img alt="Img-sm-picture" class="img-100" src="{{ asset('images/footer (1).jpg') }}"></a>
                  </li>
                  <li>
                    <a class="fancybox" data-fancybox-group="contentgallery" href="{{ asset('images/footer (2).jpg') }}"><img alt="Img-sm-picture" class="img-100" src="{{ asset('images/footer (2).jpg') }}"></a>
                  </li>
                  <li>
                    <a class="fancybox" data-fancybox-group="contentgallery" href="{{ asset('/images/footer (3).jpg') }}"><img alt="Img-sm-picture" class="img-100" src="{{ asset('images/footer (3).jpg') }}"></a>
                  </li>
                  <li>
                    <a class="fancybox" data-fancybox-group="contentgallery" href="{{ asset('/images/footer (4).jpg') }}"><img alt="Img-sm-picture" class="img-100" src="{{ asset('images/footer (4).jpg') }}"></a>
                  </li>
                  <li>
                    <a class="fancybox" data-fancybox-group="contentgallery" href="{{ asset('/images/footer (5).jpg') }}"><img alt="Img-sm-picture" class="img-100" src="{{ asset('images/footer (5).jpg') }}"></a>
                  </li>
                  <li>
                    <a class="fancybox" data-fancybox-group="contentgallery" href="{{ asset('/images/footer (6).jpg') }}"><img alt="Img-sm-picture" class="img-100" src="{{ asset('/images/footer (6).jpg') }}"></a>
                  </li>
                  <li>
                    <a class="fancybox" data-fancybox-group="contentgallery" href="{{ asset('/images/footer (7).jpg') }}"><img alt="Img-sm-picture" class="img-100" src="{{ asset('/images/footer (7).jpg') }}"></a>
                  </li>
                  <li>
                    <a class="fancybox" data-fancybox-group="contentgallery" href="{{ asset('/images/footer (8).jpg') }}"><img alt="Img-sm-picture" class="img-100" src="{{ asset('/images/footer (8).jpg') }}"></a>
                  </li>
                </ul>
                <div class="clear"></div>
              </div>
              <!-- Column -->
              <div class="col-sm-4">
                <ul class="list-link-home">
                  <li>
                    <a href="{{ url('terms') }}">Termeni si conditii</a>
                  </li>
				  <li>
                    <a href="{{ url('blog') }}">Blog</a>
                  </li>
                  <li>
                    <a href="http://www.anpc.gov.ro" target="_blank">ANPC</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="logo-dn">
              SC ATELIER CRISCAKES SRL D - J04/1346/2016 - 36698339
            </div>
            <div class="logo-dn">
              Creat cu ♥ de <a href="http://bogdanbuganu.eu" target="_blank">bogdanbuganu.eu</a>
            </div>
          </div>
        </footer>
        <!-- End Option Cake -->
      </div>
    </div>
    <script src="{{ asset('js/jquery.js') }}"></script>
    <link href="{{ asset('js/fancybox/jquery.fancybox.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('js/fancybox/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('js/slick.js') }}"></script>
    <script src="{{ asset('js/wow/wow.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/classie.js') }}"></script>
    <script src="{{ asset('js/pathLoader.js') }}"></script>
    <script src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/jquery.growl.js') }}"></script>
    <script src="{{ asset('js/cart.js') }}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script type="text/javascript">
      new WOW().init();

      $(function () {
      });
    </script>
  </body>
</html>