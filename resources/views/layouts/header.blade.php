<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="CrisCakes - minunile prind viata!" name="Toate produsele sunt realizate cu ingrediente naturale, de top: ciocolata belgiana Callebaut, frisca din smantana 35%, unt grasime 82%, oua, migdale, ulei struguri, arahide, fructe proaspete si congelate. ">
    <meta content="" name="Bogdan Buganu">
    <link href="{{ asset('images/favicon-32x32.png') }}" rel="shortcut icon">
    <title>Cris Cakes</title>
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-family.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/global.css') }}" rel="stylesheet">
    <link href="{{ asset('css/effect2.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/jquery.growl.css') }}" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
  </head>
  <body class="demo-1">
    <div class="ip-container" id="ip-container">
      <!--initial header-->
      <header class="ip-header">
        <div class="ip-loader">
          <svg class="ip-inner" height="60px" viewbox="0 0 80 80" width="60px"><path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"></path><path class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z" id="ip-loader-circle"></path></svg>
        </div>
      </header>
      <!--main content-->
      <div class="ip-main">
        <div class="top-highlight hide">
          &nbsp;
        </div>
        <!-- Start Header Cake -->
        <section class="header-wrapper">
          <header class="wrap-header">
            <div class="top-absolute">
              <div class="top-header">
                <div class="container">
                  <div class="navbar-header visible-xs">
                    <button class="navbar-toggle toggle-cake show-menu"><span class="sr-only">Meniu</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand navbar-cake" href="{{ url('/basket') }}"><img alt="Logo-CrisCakes" src="{{ asset('images/logo.png') }}"></a>
                    <a href="{{ url('/basket') }}"><i class="fas fa-shopping-cart" style="font-size:30px; padding:10px; color:#fba1a1">Cos</i></a>
				  </div>
                  <nav>
                    <ul class="header-nav hidden-xs">
                      <li>
                        <a href="{{ url('/') }}">Acasa</a>
                      </li>
                       <li>
                        <a class="show-menu" href="javascript:void(0);">Produse</a>
                      </li>
                      <li class="pad-top-0i">
                        <a href="{{ url('/') }}"><img alt="CrisCakes" src="{{ asset('images/logo-153.png') }}"></a>
                      </li>
                      <li>
                        <a href="{{ url('/gallery') }}">Galerie</a>
                      </li>
                      <li>
                        <a href="{{ url('basket') }}"><b style="color:#ffffff;">
                          <div class="notification-container {{ $cartCount > 0 ? '' : 'hidden' }}">
                            <span class="notification-bubble">{{ $cartCount }}</span>
                          </div>
                          <span class="fas fa-shopping-cart"></span>
                          Cosul Tau
                        </b></a>
                      </li>
					  
                    </ul>
                  </nav>
                  <!-- Start Mega Menu Cake -->
                  <div class="mega-menu hide">
                    <div class="tittle-mega">
                      <h4>
                        - Meniu -
                      </h4>
                    </div>
                    <div class="container">
                      <div class="row">
                        <div class="col-sm-4">
                          <ul class="list-mega">
                            <li class="bottom-red-border">
                              <a href="{{ url('/') }}">Acasa</a>
                            </li>
                          </ul>
                        </div>
                        <div class="col-sm-4">
                          <ul class="list-mega">
                            <li class="bottom-red-border">
                              Produsele noastre
                            </li>
                            @foreach ($categories as $category)
                              <li><a href="{{ url($category->slug) }}">{{ $category->name }}</a></li>
                            @endforeach
                          </ul>
                        </div>
                        <div class="col-sm-4">
                          <ul class="list-mega">
                             <li class="bottom-red-border">
                              Alte Pagini
                            </li>
                            <li>
                              <a href="{{ url('blog') }}">Blog</a>
                            </li>
                            <li>
                              <a href="{{ url('terms') }}">Termeni si conditii</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="div text-center">
                        <button class="btn btn-pink-cake mar-top-20 close-menu">Inchide</button>
                      </div>
                    </div>
                  </div>
                  <!-- End Mega Menu Cake -->
                </div>
              </div>
              <div class="triangle">
                &nbsp;
              </div>
            </div>
