<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>
<!--
  <a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->
<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="{{ asset('js/admin/jquery.js') }}"></script> 
<!--[if lt IE 9]>
<script type="text/javascript" src="js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="{{ asset('js/admin/bootstrap.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/admin/plugins/jquery-ui/jquery-ui.custom.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/caroufredsel.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/admin/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/breakpoints/breakpoints.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/dataTables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/prettyPhoto-plugin/js/jquery.prettyPhoto.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/admin/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/tagsInput/jquery.tagsinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/bootstrap-switch/bootstrap-switch.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/blockUI/jquery.blockUI.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/pnotify/js/jquery.pnotify.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/theme.js') }}"></script>
<!-- Specific Page Scripts Put Here -->
<!-- Flot Chart  -->
<script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.resize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.pie.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.categories.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.time.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/flot/jquery.flot.animator.min.js') }}"></script>
<!-- Vector Map -->
<script type="text/javascript" src="{{ asset('js/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- Calendar -->
<script type="text/javascript" src="{{ asset('js/admin/plugins/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/jquery-ui/jquery-ui.custom.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<!-- Intro JS (Tour) -->
<script type="text/javascript" src="{{ asset('js/admin/plugins/introjs/js/intro.min.js') }}"></script>
<!-- Sky Icons -->
<script type="text/javascript" src="{{ asset('js/admin/plugins/skycons/skycons.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/admin/plugins/bootstrap-wysiwyg/js/wysihtml5-0.3.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/bootstrap-wysiwyg/js/bootstrap-wysihtml5-0.0.2.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/admin/plugins/tagsInput/jquery.tagsinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/plugins/bootstrap-switch/bootstrap-switch.min.js') }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</html>