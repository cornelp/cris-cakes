<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Admin</title>
    <meta name="keywords" content="Admin " />
    <meta name="description" content="Admin">
    <meta name="author" content="Bogdan Buganu">
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('images/ico/apple-touch-icon-57-precomposed.png') }}">
    <link rel="shortcut icon" href="images/ico/favicon.png">
    <!-- CSS -->
    <!-- Bootstrap & FontAwesome & Entypo CSS -->
    <link href="{{ asset('css/admin/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <!--[if IE 7]>
    <link type="text/css" rel="stylesheet" href="css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="{{ asset('css/admin/font-entypo.css') }}" rel="stylesheet" type="text/css">
    <!-- Fonts CSS -->
    <link href="{{ asset('css/admin/fonts.css') }}"  rel="stylesheet" type="text/css">
    <!-- Plugin CSS -->
    <link href="{{ asset('js/admin/plugins/jquery-ui/jquery-ui.custom.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/prettyPhoto-plugin/css/prettyPhoto.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/isotope/css/isotope.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/pnotify/css/jquery.pnotify.css') }}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/google-code-prettify/prettify.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/mCustomScrollbar/jquery.mCustomScrollbar.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/tagsInput/jquery.tagsinput.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/bootstrap-switch/bootstrap-switch.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css">
    <!-- Specific CSS -->
    <link href="{{ asset('js/admin/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/fullcalendar/fullcalendar.print.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/admin/plugins/introjs/css/introjs.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('js/admin/plugins/bootstrap-switch/bootstrap-switch.css') }}" rel="stylesheet" type="text/css">
<!-- Specific CSS -->
	<link href="{{ asset('js/admin/plugins/jquery-file-upload/css/jquery.fileupload.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('js/admin/plugins/jquery-file-upload/css/jquery.fileupload-ui.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('js/admin/plugins/bootstrap-wysiwyg/css/bootstrap-wysihtml5-0.0.2.css') }}" rel="stylesheet" type="text/css">
     <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="{{ asset('css/admin/theme.min.css') }}" rel="stylesheet" type="text/css">
    <!--[if IE]>
    <link href="css/ie.css" rel="stylesheet" >
    <![endif]-->
    <link href="{{ asset('css/admin/chrome.css') }}" rel="stylesheet" type="text/chrome">
    <!-- chrome only css -->
    <!-- Responsive CSS -->
    <link href="{{ asset('css/admin/theme-responsive.min.css') }}" rel="stylesheet" type="text/css">
    <!-- for specific page in style css -->
    <!-- for specific page responsive in style css -->
    <!-- Head SCRIPTS -->
    <script src="{{ asset('js/admin/modernizr.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/admin/mobile-detect.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/admin/mobile-detect-modernizr.js') }}" type="text/javascript"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/html5shiv.js"></script>
    <script type="text/javascript" src="js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="dashboard" class="full-layout  nav-right-hide nav-right-start-hide  nav-top-fixed      responsive    clearfix" data-active="dashboard "  data-smooth-scrolling="1">
    <div class="vd_body">
    <!-- Header Start -->
    <header class="header-1" id="header">
      <div class="vd_top-menu-wrapper">
        <div class="container ">
          <div class="vd_top-nav vd_nav-width  ">
            <div class="vd_panel-header">
              <div class="logo">
                <a href="{{ url('/') }}"><img alt="logo" src="{{ asset('images/logo.png') }}" style="height: 40px;"></a>
              </div>
              <!-- logo -->
              <div class="vd_panel-menu  hidden-sm hidden-xs" data-intro="<strong>Minimize Left Navigation</strong><br/>" data-step=1>
                <span class="nav-medium-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Medium Nav Toggle" data-action="nav-left-medium">
                <i class="fa fa-bars"></i>
                </span>
                <span class="nav-small-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Small Nav Toggle" data-action="nav-left-small">
                <i class="fa fa-ellipsis-v"></i>
                </span>
              </div>
              <div class="vd_panel-menu left-pos visible-sm visible-xs">
                <span class="menu" data-action="toggle-navbar-left">
                <i class="fa fa-ellipsis-v"></i>
                </span>
              </div>
              <div class="vd_panel-menu visible-sm visible-xs">
                <span class="menu visible-xs" data-action="submenu">
                <i class="fa fa-bars"></i>
                </span>
              </div>
              <!-- vd_panel-menu -->
            </div>
            <!-- vd_panel-header -->
          </div>
          <div class="vd_container">
            <div class="row">
              <div class="col-sm-5 col-xs-12">
              </div>
              <div class="col-sm-7 col-xs-12">
                <div class="vd_mega-menu-wrapper">
                  <div class="vd_mega-menu pull-right">
                    <ul class="mega-ul">
                      <li id="top-menu-profile" class="profile mega-li">
                        <a href="#" class="mega-link"  data-action="click-trigger">
                        <span  class="mega-image">
                        </span>
                        <span class="mega-name">
                        Admin CrisCakes <i class="fa fa-caret-down fa-fw"></i>
                        </span>
                        </a>
                        <div class="vd_mega-menu-content  width-xs-2  left-xs left-sm" data-action="click-target">
                          <div class="child-menu">
                            <div class="content-list content-menu">
                              <ul class="list-wrapper pd-lr-10">
                                <li>
                                  <a href="#">
                                    <div class="menu-icon"><i class=" fa fa-sign-out"></i></div>
                                    <div class="menu-text">Delogare</div>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <!-- Head menu search form ends -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- container -->
      </div>
      <!-- vd_primary-menu-wrapper -->
    </header>
    <!-- Header Ends -->
    <div class="content">
    <div class="container">
    <div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left  ">
      <div class="navbar-menu clearfix">
        <div class="vd_menu">
          <ul>
            <li>
              <a href="javascript:void(0);" data-action="click-trigger">
              <span class="menu-icon"><i class="icon-newspaper"></i></span>
              <span class="menu-text">Comenzi</span>
              <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
              </a>
              <div class="child-menu"  data-action="click-target">
                <ul>
                  <li>
                    <a href="{{ url('/admin/orders-unprocessed') }}">
                    <span class="menu-text">Comenzi noi</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/admin/orders-processed') }}">
                    <span class="menu-text">Comenzi procesate</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/admin/manual-orders/create') }}">
                    <span class="menu-text">Creeaza o comanda noua</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
			<li>
              <a href="javascript:void(0);" data-action="click-trigger">
              <span class="menu-icon"><i class="icon-newspaper"></i></span>
              <span class="menu-text">Galerie</span>
              <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
              </a>
              <div class="child-menu"  data-action="click-target">
                <ul>
                  <li>
                    <a href="{{ url('/admin/gallery') }}">
                    <span class="menu-text">Galerie Foto</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
			<li>
              <a href="javascript:void(0);" data-action="click-trigger">
              <span class="menu-icon"><i class="icon-newspaper"></i></span>
              <span class="menu-text">Produse</span>
              <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
              </a>
              <div class="child-menu"  data-action="click-target">
                <ul>
                  <li>
                    <a href="{{ url('/admin/categories') }}">
                    <span class="menu-text">Categorii</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/admin/products') }}">
                    <span class="menu-text">Lista produse</span>
                    </a>
                  </li>
				  <li>
                    <a href="{{ url('/admin/products/create') }}">
                    <span class="menu-text">Adauga Produs</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
			<li>
              <a href="javascript:void(0);" data-action="click-trigger">
              <span class="menu-icon"><i class="icon-newspaper"></i></span>
              <span class="menu-text">Compozitii</span>
              <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
              </a>
              <div class="child-menu"  data-action="click-target">
                <ul>
                  <li>
                    <a href="{{ url('/admin/structures') }}">
                    <span class="menu-text">Lista</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/admin/structures/create') }}">
                    <span class="menu-text">Adauga</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
			<li>
              <a href="javascript:void(0);" data-action="click-trigger">
              <span class="menu-icon"><i class="icon-newspaper"></i></span>
              <span class="menu-text">Numar Portii</span>
              <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
              </a>
              <div class="child-menu"  data-action="click-target">
                <ul>
                  <li>
                    <a href="{{ url('/admin/servings') }}">
                    <span class="menu-text">Lista</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/admin/servings/create') }}">
                    <span class="menu-text">Adauga</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
			<li>
              <a href="javascript:void(0);" data-action="click-trigger">
              <span class="menu-icon"><i class="icon-newspaper"></i></span>
              <span class="menu-text">Toppere</span>
              <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
              </a>
              <div class="child-menu"  data-action="click-target">
                <ul>
                  <li>
                    <a href="{{ url('/admin/toppers') }}">
                    <span class="menu-text">Lista</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('admin/toppers/create') }}">
                    <span class="menu-text">Adauga</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
			<li>
              <a href="javascript:void(0);" data-action="click-trigger">
              <span class="menu-icon"><i class="icon-newspaper"></i></span>
              <span class="menu-text">Configurare</span>
              <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
              </a>
              <div class="child-menu"  data-action="click-target">
                <ul>
                  <li>
                    <a href="{{ url('/admin/config') }}">
                    <span class="menu-text">Lista</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
			<li>
				<a href="{{ url('/admin/announce') }}">
				<span class="menu-text"><span class="menu-icon"><i class="icon-newspaper"></i></span> Anunt Site</span>
				</a>
			</li>
			<li>
				<a href="{{ url('/admin/invalid-dates') }}">
				<span class="menu-text"><span class="menu-icon"><i class="icon-newspaper"></i></span> Perioada inchidere</span>
				</a>
			</li>
          </ul>
          <!-- Head menu search form ends -->
        </div>
      </div>
      <div class="navbar-spacing clearfix">
      </div>
    </div>
    <!-- Middle Content Start -->
