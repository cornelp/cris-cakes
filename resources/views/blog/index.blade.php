@extends('layouts.master')

@section('content')
<div class="tittle-sub-top pad-top-150">
    <div class="container">
        <a href="{{ url('/') }}">Acasa</a> /
        <h1> Blog </h1>
    </div>
</div>
</header>
<!-- End Content Header -->
<div class="blue-arrow">
    &nbsp;
</div>
<!-- Start Blog Content -->
<div class="blog-cake">
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-2 col-sm-8 overflow-xs">
                <div class="wrap-blog-content">
                    <div class="left-date-blog">
                        <h1>
                            14
                        </h1>
                        <p>
                            Mai 2017
                        </p>
                    </div>
                    <div class="right-hand-content-blog">
                        <img alt="Cakes Co" class="img-100" src="{{ asset('images/blog1.jpg') }}">
                        <article class="content-blog-bottom">
                            <div class="row">
                                <div class="col-sm-8">
                                    <h4>
                                        Cristina <span>Gavriliu</span>
                                    </h4>
                                    <p>
                                        CrisCakes, povestea dulce a unui cofetar, fost director executiv de agenție bancară
                                    </p>
                                </div>
                                <div class="col-sm-4">
                                    <div class="right-read text-center">
                                        <span><span class="btn btn-blue-cake btn-1">Citeste mai mult</span><span class="btn btn-blue-cake btn-2 hide">Mai putin</span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="show-content-blog show-1">
                                <hr>
                                <a href="javascript:void(0);">Cris Cakes</a><br>Te întâmpină din pragul casei (a cărei bucătărie
                                a devenit de câteva luni laborator de cofetărie cu acte în regulă), cu atâta bucurie, că
                                e imposibil să nu rămâi în prima clipă uimit, ca imediat după, oricât de mari ți-ar fi supărările
                                doar de tine știute, să te lași învăluit tu însuți într-o bucurie aparent fără motiv. Înaintând
                                apoi, pe aleea care duce la ”fereastra” laboratorului, prin care se livrează produsul clienților,
                                te izbește un miros de bunătăți care te întoarce instantaneu în copilărie, în casa vreunei
                                bunici sau mătuși pe care le iubeai grozav pentru prăjiturile pe care, cu atâta dragoste,
                                le făceau pentru tine. Laboratorul amenajat în fosta bucătărie a casei are și un nume ”dulce”:
                                CrisCakes. Iar cea care te întâmpină mereu debordând de bucurie și energie este chiar ”mama”
                                laboratorului (și ”mama” echipei cu care lucrează, echipa însăși numind-o așa!), Cristina
                                Gavriliu.
                                <br>
                                <br> Puteti citi articolul complet pe <a href="https://www.desteptarea.ro/criscakes-povestea-dulce-a-unui-cofetar-fost-director-executiv-de-agentie-bancara/"
                                    target="_blank">desteptarea.ro aici</a>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="wrap-blog-content">
                    <div class="left-date-blog">
                        <h1>
                            10
                        </h1>
                        <p>
                            Mai 2017
                        </p>
                    </div>
                    <div class="right-hand-content-blog">
                        <img alt="Cakes" class="img-100" src="{{ asset('images/blog2.jpg') }}">
                        <article class="content-blog-bottom">
                            <div class="row">
                                <div class="col-sm-8">
                                    <h4>
                                        O afacere <span>dulce</span> ca ea
                                    </h4>
                                    <p>
                                        Un director de filială bancară poate avea ca hobby, prăjiturile.
                                    </p>
                                </div>
                                <div class="col-sm-4">
                                    <div class="right-read text-center">
                                        <span><span class="btn btn-blue-cake btn-3">Mai mult</span><span class="btn btn-blue-cake btn-4 hide">Mai putin</span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="show-content-blog show-2">
                                <hr>
                                <a href="javascript:void(0);">ARTĂ ABSOLUTĂ </a><br> Dar să renunțe la un post de conducere
                                bine plătit, pentru că deodată a simțit o pasiune pentru dulciuri, e mai mult decât curaj,
                                ținând cont că toată viața a cumpărat si nu a gătit nimic dulce. Cine ar fi acordat șanse
                                unei asemenea decizii? Sinceră să fiu, aş fi fost reticentă și probabil aş fi descurajat-o
                                pe Cristina Gavriliu. Dar dacă n-a întâlnit piedici și a mizat pe propriul feeling, a început
                                timid să facă prăjituri pe care le dăruia prietenilor, așteptând feedback-ul cu sufletul
                                la gură. În urmă cu ceva timp, m-am întâlnit cu ea si pe fugă mi-a oferit o cutie de prăjituri:
                                „Sunt făcute de mine. Să-mi zici cum ți se par!”.
                                <br>
                                <br> Puteti citi articolul complet pe <a href="http://www.bacaoanu.com/o-afacere-dulce-ca-ea/"
                                    target="_blank">bacaoanu.com aici</a>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="wrap-blog-content">
                    <div class="left-date-blog">
                        <h1>
                            18
                        </h1>
                        <p>
                            Martie 2017
                        </p>
                    </div>
                    <div class="right-hand-content-blog">
                        <img alt="Cakes-co2" class="img-100" src="{{ asset('images/blog3.jpg') }}">
                        <article class="content-blog-bottom">
                            <div class="row">
                                <div class="col-sm-8">
                                    <h4>
                                        Cris<span>Cakes</span>
                                    </h4>
                                    <p>
                                        Un curcubeu al aromelor...
                                    </p>
                                </div>
                                <div class="col-sm-4">
                                    <div class="right-read text-center">
                                        <span><span class="btn btn-blue-cake btn-5">Mai mult</span><span class="btn btn-blue-cake btn-6 hide">Mai putin</span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="show-content-blog show-3">
                                <hr>
                                <a href="javascript:void(0);">Amprenta proprie!</a><br>Ca o explozie de arome, culori vii
                                și fructe. Nu foarte dulce, totul răcoritor, iar fiecare bucățică te îndeamnă la degustare.
                                Nu-s doar prăjituri, torturi, tarte. Dintr-astea găsești oriunde, la prețuri pentru toate
                                buzunarele. Ceva mai bun în materie de cofetărie, n-am gustat niciodată. Toate au și-o poveste
                                care pornește de la o pasiune descoperită mai târziu. Cristina Gavriliu, de la CrisCakes,
                                a descoperit patiseria dintr-o cu totul altă lume: cea bancară.
                                <br>
                                <br> Puteti citi articolul complet pe <a href="http://fermapolitica.ro/2017/03/18/un-curcubeu-al-aromelor-criscakes/"
                                    target="_blank">fermapolitica.ro aici</a>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Blog Content -->
</section>
@endsection