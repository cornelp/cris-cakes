<!DOCTYPE html>
<html>
<head>
    <style> th, td { border-bottom: 1px solid #ddd; padding: 8px; }</style>
</head>
<body>
    <p>
       Stimate Client {{ $order->name }}, <br /><br />
       Va multumim pentru achizitia de la <a href="https://criscakes.ro/">www.criscakes.ro</a> <br />
       Va confirmam ca am primit comanda dumneavoastra. In continuare, va atasam detaliile comenzii.
    </p>

 <br /><br />

<table>
    <thead>
        <tr style="color: grey; font-style: italic;">
            <th>Nume produs</th>
            <th>Compozitie</th>
            <th>Culoare</th>
            <th>Portie</th>
            <th>Topper</th>
            <th>Cantitate</th>
            <th>Pret</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($order->details as $order)
            <tr>
                <td>{{ $order->product->name }}</td>
                <td>{{ optional($order->structure)->name ?? '-' }}</td>
                <td>{{ $order->color ?? '-' }}</td>
                <td>{{ $order->serving ? $order->serving->name : '-' }}</td>
                <td>{{ $order->topper ? $order->topper->name : '-' }}</td>
                <td>{{ $order->quantity }}</td>
                <td>@float($order->price)</td>
            </tr>
        @endforeach
    </tbody>
</table>

    <br><br>

    <div style="color: grey; font-style: italic;">
        {{-- Total @float($order->details->sum('price')) RON --}}
        <br>
    </div>

    <br /><br />

    <p>
        <span style="color: red;">Acest mesaj este automat, va rugam sa nu raspundeti la acest email.</span>
        Urmeaza sa fiti contactat telefonic pentru confirmarea comenzii.<br><br>
        Cu stima,<br />Cris Cakes
    </p>
</body>
</html>
