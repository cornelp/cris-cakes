Buna!
<br><br>

<p>O noua comanda a fost primita.</p>

<table>
    <thead>
        <tr style="color: grey; font-style: italic; border:1px solid #000;">
            <th>Nume produs</th>
            <th>Compozitie</th>
            <th>Culoare</th>
            <th>Portie</th>
            <th>Topper</th>
            <th>Cantitate</th>
            <th>Pret</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($order->details as $order)
            <tr style="border:1px solid #000;">
                <td>{{ $order->product->name }}</td>
                <td>{{ optional($order->structure)->name ?? '-' }}</td>
                <td>{{ $order->color ?? '-' }}</td>
                <td>{{ $order->serving ? $order->serving->name : '-' }}</td>
                <td>{{ $order->topper ? $order->topper->name : '-' }}</td>
                <td>{{ $order->quantity }}</td>
                <td>@float($order->price)</td>
            </tr>
        @endforeach
    </tbody>
</table>

<br><br>
Pentru a accesa platforma apasati click aici ->>> <a href="criscakes.ro/admin/orders-unprocessed" target="_blank">Comenzi noi</a>