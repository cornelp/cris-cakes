@extends('layouts.master')

@section('content')
<div class="tittle-sub-top pad-top-150">
    <div class="container">
        <a href="{{ url('/') }}">Acasa</a> /
        <h1> Galerie Foto </h1>
    </div>
</div>
</header>
<div class="green-arrow">
    &nbsp;
</div>
</section>
<!-- End Header Cake -->
<!-- Start Gallery -->
<section class="gallery">
    <h2 class="hide">
        &nbsp;
    </h2>
    <div class="chart-cake">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="row gallery-cake">
                        @if ($images->count())
                            @foreach ($images as $image)
                                <div class="col-xs-12 col-sm-4 mar-btm-20">
                                    <a class="fancybox" data-fancybox-group="contentgallerygel" href="{{ asset('images/gallery/' . $image->url) }}">
                                        <div class="gal-relative">
                                            <div class="gal-absolute">
                                                &nbsp;
                                            </div>
                                            <img alt="Gallery-Picture" class="img-100" src="{{ asset('images/gallery/' . $image->url) }}">
                                        </div>
                                        <h4>
                                            {{ $image->name }}
                                        </h4>
                                    </a><!--{{ $image->description }}-->
                                </div>
                            @endforeach
                        @else
                            <div class="alert alert-info">Nu este nicio poza aici.</div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Gallery -->
<!-- Start Footer Cake -->
@endsection