@extends('layouts.master')

@section('content')
<div class="tittle-cake text-center pad-top-150">
    <div class="container">
        <h2>Bine ai venit in</h2>
        <h1>Atelierul CrisCakes</h1>
    </div>
</div>

@if ($announce->active)
    @include('modal')
@endif

<div class="slider-cake">
    <div class="container pad-md-100">
        <div class="center">
			<div>
                <img alt="Cake-Four" src="{{ asset('images/sliDER.png') }}" style="width:300px; height:300px;">
            </div>
			<div>
                <img alt="Cake-Four" src="{{ asset('images/slider (3).png') }}" style="width:300px; height:300px;">
            </div>
			<div>
                <img alt="Cake-Four" src="{{ asset('images/slider (5).png') }}" style="width:300px; height:300px;">
            </div>
            <div>
                <img alt="Cake-Four" src="{{ asset('images/slider (2).png') }}" style="width:300px; height:300px;">
            </div>
			<div>
                <img alt="Cake-Four" src="{{ asset('images/slider (3).png') }}" style="width:300px; height:300px;">
            </div>
			<div>
                <img alt="Cake-Four" src="{{ asset('images/slider (4).png') }}" style="width:300px; height:300px;">
            </div>
			<div>
                <img alt="Cake-Four" src="{{ asset('images/slider (5).png') }}" style="width:300px; height:300px;">
            </div>
			<div>
                <img alt="Cake-Four" src="{{ asset('images/slider (6).png') }}" style="width:300px; height:300px;">
            </div>
			<div>
                <img alt="Cake-Four" src="{{ asset('images/slider (1).png') }}" style="width:300px; height:300px;">
            </div>
        </div>
    </div>
</div>
<div class="green-table mar-to-top">
    &nbsp;
</div>
<div class="green-arrow">
    &nbsp;
</div>
</header>
</section>
<!-- End Header Cake -->
<!-- Start About Cake -->
<section class="about-cake">
    <div class="container">
        <!-- About Content -->
        <h2 class="hide">
            &nbsp;
        </h2>
        <div class="about-content">
            <img alt="Cake-White" src="{{ asset('images/cake-white.png') }}">
            <p>
                Toate produsele sunt realizate cu ingrediente naturale, de top: ciocolata belgiana Callebaut, frisca din smantana 35%, unt
                grasime 82%, oua, migdale, ulei struguri, arahide, fructe proaspete si congelate.
                <br>Insertiile de fructe, sunt gatite in laborator, crema de vanilie cu galbenusuri, gatita la foc usurel
                in laborator. Pralinele si crocantul sunt realizate in laborator din migdale, alune, sau crocant copt in
                cuptor.
                <br>Glazurile sunt tip oglinda, dupa retete si tehnici noi in cofetarie.
                <br>Glazura de tip velvet este o glazura facuta din unt de cacao si ciocolata Callebaut si se aplica prin
                tehnica suflatului cu un pistol special.
                <br>In Atelierul Criscakes nu se foloseste nici un tip de mixuri sau premixuri gata preparate.
                <br>In fiecare bucatica de dulce, Criscakes foloseste ingrediente secrete, ca: un strop de suflet, bunatate,
                bucurie, zambet si disponibilitate.
            </p>
        </div>
    </div>
</section>
<!-- End About Cake -->
<!-- Start Product Cake -->
<section class="product-cake">
    <div class="container">
        <!-- Product Tittle -->
        <div class="product-tittle">
            <img alt="Cake-Purple" src="{{ asset('images/cake-purple.png') }}">
            <h2>
                Produse
            </h2>
        </div>
        <!-- Product Content -->
        <div class="product-content">
            <div class="row">
                <!-- Column -->
                <div class="col-sm-4">
                    <div class="wrap-product">
                        <div class="top-product blue-cake">
                            <p class="mar-top-10 mar-btm-0">
                                Torturi
                            </p>
                        </div>
                        <div class="bottom-product bottom-blue">
                            <div class="bottom-product-abs blue-dot">
                                <div class="button-cake">
                                    <div class="blue-button-cake">
                                        <button class="button-d-cake blue-button-cake"><a href="{{ url('tort-cu-forma-speciala') }}">Vezi mai multe</a></button>
                                    </div>
                                </div>
                            </div>
                            <div class="wrap-bottom-cake">
                                <p>
                                    Comanda acum tortul dorit! Nu mai sta pe ganduri
                                </p>
                                <div class="blue-line"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-sm-4">
                    <div class="wrap-product">
                        <div class="top-product red-cake">
                            <p class="mar-top-10 mar-btm-0">
                                Prajituri
                            </p>
                        </div>
                        <div class="bottom-product bottom-red">
                            <div class="bottom-product-abs pink-dot">
                                <div class="button-cake">
                                    <div class="blue-button-cake">
                                        <button class="button-d-cake blue-button-cake"><a href="{{ url('prajituri') }}">Vezi mai multe</a></button>
                                    </div>
                                </div>
                            </div>
                            <div class="wrap-bottom-cake">
                                <p>
                                    Avem cele mai delicioase prajituri! Vino si convinge-te!
                                </p>
                                <div class="red-line"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-sm-4">
                    <div class="wrap-product">
                        <div class="top-product orange-cake">
                            <p class="mar-top-10 mar-btm-0">
                                Platouri
                            </p>
                        </div>
                        <div class="bottom-product bottom-orange">
                            <div class="bottom-product-abs orange-dot">
                                <div class="button-cake">
                                    <div class="blue-button-cake">
                                        <button class="button-d-cake blue-button-cake"><a href="{{ url('platouri') }}">Vezi mai multe</a></button>
                                    </div>
                                </div>
                            </div>
                            <div class="wrap-bottom-cake">
                                <p>
                                    Personalizeaza-ti platoul cu prajiturile care iti plac.
                                </p>
                                <div class="orange-line"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column Tittle -->
                <div class="col-sm-12">
                    <p class="text-content text-center">
                        Cu pasiune, zambete si bucurie va invit sa gustati prajituri din <b class="purple-color">bucataria mea!</b>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="news-cake">
    <div class="triangle-no-animate">
        &nbsp;
    </div>
    <!-- News Content -->
    <div class="new-cake-content mar-top-20">
        <!-- Tittle News Content -->
        <div class="tittle-cake text-center">
            <div class="container">
                <img alt="Cake-White" src="{{ asset('images/cake-purple.png') }}">
                <h2>Cristina Gavriliu</h2>
            </div>
        </div>
        <!-- Content News-->
        <div class="container mar-top-20">
            <div class="row">
                <div class="col-sm-6 no-pad-right">
                    <div class="left-news">
                    </div>
                    <div class="right-news">
                        <div class="text-table">
                            <p>
                                <a href="shop.html"><span class="discount">Paris<span class="percent"></span><br></span><span class="sale">Capitala cofetariei!</span></a>
                            </p>
                        </div>
                        <div class="text-table dot-background">
                            <p>
                                <img alt="Client" src="{{ asset('images/paris2.jpg') }}" style="width:285px;height:285px;">
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 no-pad-left">
                    <div class="top-news-right">
                        <div class="left-news-right">
                            <div class="text-table">
                                <a class="fancybox" data-fancybox-group="contentnews" href="{{ asset('images/paris4.jpg') }}">
                                    <div class="wizz-effect wizz-orange">
                                    </div>
                                </a>
                                <p>
                                    <img alt="Ice Cream" class="img-100" src="{{ asset('images/paris4.jpg') }}">
                                </p>
                            </div>
                        </div>
                        <div class="right-news-right">
                            <div class="text-table">
                                <a class="fancybox" data-fancybox-group="contentnews" href="{{ asset('images/paris3.jpg') }}">
                                    <div class="wizz-effect wizz-green">
                                    </div>
                                </a>
                                <p>
                                    <img alt="Ice Cream Cake" class="img-100" src="{{ asset('images/paris3.jpg') }}">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="bottom-new-right">
                        <div class="quote">
                            <div>
                                <span class="mar-right-10"><img alt="Quote" class="Quote" src="{{ asset('images/quote.png') }}"></span>
                                <p>
                                    M-am delectat cu cele mai fine gusturi, de la cei mai mari chefi patiseri si am invatat secrete ale cofetariei!
                                </p>
                            </div>
                            <div>
                                <span class="mar-right-10"><img alt="Quote" class="Quote" src="{{ asset('images/quote.png') }}"></span>
                                <p>
                                    Sunt recunoscatoare si fericita pentru intamplarile frumoase din viata mea!
                                </p>
                            </div>
                            <div>
                                <span class="mar-right-10"><img alt="Quote" class="Quote" src="{{ asset('images/quote.png') }}"></span>
                                <p>
                                    Multumesc, Mirela! A fost o onoare sa lucram impreuna, sa-mi dovedesti inca odata calitatile pentru care esti atat de apreciata:
                                    cunostintele, profesionalismul si modestia ta.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Content News-->
    </div>
    <!-- End News Content-->
</section>
<section class="option">
    <!-- Tittle Option -->
    <div class="green-table pad-top-10 pad-btm-10">
        <div class="container">
            <div class="tittle-cake text-center">
                <img alt="Cake-White" src="{{ asset('images/cake-white.png') }}">
                <h2>Ce putem face</h2>
            </div>
        </div>
    </div>
    <div class="green-arrow"></div>
    <!-- Option Content -->
    <div class="option-content">
        <div class="container">
            <!-- Column -->
            <div class="col-sm-4">
                <div class="messes">
                    <div class="messes-show"></div>
                    <div class="round-wrap green-option"></div>
                </div>
                <h4 class="green-color">
                    Nunti / Botez
                </h4>
                <div class="line-temp line-green-sm">
                    &nbsp;
                </div>
                <p class="text-center mar-top-10">
                    Cris Cakes te ajută cu deserturile pe care ți le dorești. Vom face ca viziunea ta să devină deserturile de care invitații
                    să dorească să nu se despartă.
                </p>
            </div>
            <!-- Column -->
            <div class="col-sm-4">
                <div class="messes">
                    <div class="messes-show"></div>
                    <div class="round-wrap orange-option"></div>
                </div>
                <h4 class="orange-color">
                    Aniversari
                </h4>
                <div class="line-temp line-orange-sm">
                    &nbsp;
                </div>
                <p class="text-center mar-top-10">
                    Suntem specialiști în toate aspectele planificării, echilibrul potrivit de arome al fructelor de sezon, decorul atent ales
                    și culorile care dau tonul perfect.
                </p>
            </div>
            <!-- Column -->
            <div class="col-sm-4">
                <div class="messes">
                    <div class="messes-show"></div>
                    <div class="round-wrap blue-option"></div>
                </div>
                <h4 class="blue-color">
                    Produse sanatoase
                </h4>
                <div class="line-temp line-blue-sm">
                    &nbsp;
                </div>
                <p class="text-center mar-top-10">
                    Am adăugat ingrediente proaspete şi multă pasiune la realizarea acestor preparate, care sperăm noi, vor ajunge în casele
                    voastre.
                </p>
            </div>
            <!-- Column -->
            <div class="col-sm-4">
                <div class="messes">
                    <div class="messes-show"></div>
                    <div class="round-wrap pink-option"></div>
                </div>
                <h4 class="pink-color">
                    Evenimente Corporate
                </h4>
                <div class="line-temp line-pink-sm">
                    &nbsp;
                </div>
                <p class="text-center mar-top-10">
                    Suntem aici cand vrei să-ţi surprinzi colegii de la birou, echipa CrisCakes ofera servicii pentru companiile localizate în
                    Bacau.
                </p>
            </div>
            <!-- Column -->
            <div class="col-sm-4">
                <div class="messes">
                    <div class="messes-show"></div>
                    <div class="round-wrap purple-option"></div>
                </div>
                <h4 class="purple-color">
                    Degustari
                </h4>
                <div class="line-temp line-purple-sm">
                    &nbsp;
                </div>
                <p class="text-center mar-top-10">
                    Oferim sansa clientilor nostri sa deguste inainte de a da comanda.
                </p>
            </div>
            <!-- Column -->
            <div class="col-sm-4">
                <div class="messes">
                    <div class="messes-show"></div>
                    <div class="round-wrap dpurple-option"></div>
                </div>
                <h4 class="dpurple-color">
                    Gourmet
                </h4>
                <div class="line-temp line-dpurple-sm">
                    &nbsp;
                </div>
                <p class="text-center mar-top-10">
                    Clasic sau modern, la o pensiune retrasă sau într-un restaurant central, restrâns sau de anvergură – CrisCakes te ajută cu
                    deserturile pe care ți le dorești.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End Option Cake -->
<!-- Start Pricing Cake -->
<section class="pricing-cake">
    <div class="triangle-no-animate">
        &nbsp;
    </div>
    <!-- Content Pricing Cake -->
    <div class="content-pricing-cake">
        <div class="tittle-cake text-center">
            <div class="container">
                <img alt="Cake-White" src="{{ asset('images/cake-white.png') }}">
                <h2>
                    Pret Torturi
                </h2>
            </div>
        </div>
        <div class="container mar-top-20">
            <!-- Column -->
            <div class="col-sm-2 mar-btm-20">
                <div class="img-wrap-price">
                    <img alt="Price-Purple" class="img-full-sm" src="{{ asset('images/price-purple.png') }}">
                </div>
                <div class="content-price content-price-tag text-center">
                    <h4 class="dpurple-color">
                        6-8<span>Portii</span>
                    </h4>
                    <div class="price-purple">
                        <div class="triangle-no-animate">
                            &nbsp;
                        </div>
                        <div class="text-price">
                            65 RON
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-sm-2 mar-btm-20">
                <div class="img-wrap-price">
                    <img alt="Price-Pink" class="img-full-sm" src="{{ asset('images/price-pink.png') }}">
                </div>
                <div class="content-price content-price-tag text-center">
                    <h4 class="pink-color">
                        8-10<span>Portii</span>
                    </h4>
                    <div class="price-pink">
                        <div class="triangle-no-animate">
                            &nbsp;
                        </div>
                        <div class="text-price">
                            75 RON
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-sm-2 mar-btm-20">
                <div class="img-wrap-price">
                    <img alt="Price-Green" class="img-full-sm" src="{{ asset('images/price-green.png') }}">
                </div>
                <div class="content-price content-price-tag text-center">
                    <h4 class="green-color">
                        10-12<span>Portii</span>
                    </h4>
                    <div class="price-green">
                        <div class="triangle-no-animate">
                            &nbsp;
                        </div>
                        <div class="text-price">
                            95 RON
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-sm-2 mar-btm-20">
                <div class="img-wrap-price">
                    <img alt="Price-Blue" class="img-full-sm" src="{{ asset('images/price-blue.png') }}">
                </div>
                <div class="content-price content-price-tag text-center">
                    <h4 class="blue-color">
                        15<span>Portii</span>
                    </h4>
                    <div class="price-blue">
                        <div class="triangle-no-animate">
                            &nbsp;
                        </div>
                        <div class="text-price">
                            120 RON
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-sm-2 mar-btm-20">
                <div class="img-wrap-price">
                    <img alt="Price-Blue" class="img-full-sm" src="{{ asset('images/price-purple.png') }}">
                </div>
                <div class="content-price content-price-tag text-center">
                    <h4 class="purple-color">
                        20<span>Portii</span>
                    </h4>
                    <div class="price-purple">
                        <div class="triangle-no-animate">
                            &nbsp;
                        </div>
                        <div class="text-price">
                            140 RON
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-sm-2 mar-btm-20">
                <div class="img-wrap-price">
                    <img alt="Price-Blue" class="img-full-sm" src="{{ asset('images/price-pink.png') }}">
                </div>
                <div class="content-price content-price-tag text-center">
                    <h4 class="pink-color">
                        25<span>Portii</span>
                    </h4>
                    <div class="price-pink">
                        <div class="triangle-no-animate">
                            &nbsp;
                        </div>
                        <div class="text-price">
                            170 RON
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="triangle-top-no-animate">
        &nbsp;
    </div>
</section>
<!-- End Pricing Cake -->
<!-- Start Team Cake -->
<section class="abouts-cake">
    <div class="tittle-cake text-center">
        <div class="container">
            <img alt="Cake-Pink" src="{{ asset('images/cake-pink.png') }}">
            <h2 class="pink-color">
                Echipa Noastra
            </h2>
        </div>
    </div>
    <div class="container mar-top-20">
        <!-- Column -->
        <div class="col-sm-12">
            <div class="img-round-about">
                <img alt="About Team" class="img-100" src="{{ asset('images/about-11.png') }}">
            </div>
            <h4>
                Cristina
            </h4>
            <div class="line-pink-about">
                &nbsp;
            </div>
            <p class="text-center">
                Nu poti cumpara fericirea <br>dar poti cumpara prajituri! <br>Si pentru mine e acelasi lucru!
            </p>
        </div>
        <!-- Column -->
    </div>
    <div class="tittle-cake text-center mar-top-20" id="contact-form">
        <div class="container">
            <img alt="Cake-Pink" src="{{ asset('images/cake-pink.png') }}">
            <h2 class="pink-color">
                Contacteaza-ne!
            </h2>
        </div>
    </div>
    <div class="container mar-top-20">
        <div class="col-sm-offset-3 col-sm-6">
            <div class="form-group">
                <input class="form-control form-default-cakes" placeholder="Nume" type="text">
            </div>
            <div class="form-group">
                <input class="form-control form-default-cakes" placeholder="Email" type="email">
            </div>
            <div class="form-group">
                <input class="form-control form-default-cakes" placeholder="Telefon" type="phone">
            </div>
            <textarea class="form-control form-default-cakes" placeholder="Mesajul tau"></textarea>
            <div class="form-group">
                <button class="btn btn-lg btn-pink-cake btn-send mar-top-20">Trimite</button>
            </div>
        </div>
    </div>
</section>
<!-- End Option Cake -->
<!-- Start Footer Cake -->

@endsection

@section('js')
    @if ($announce->active)
    <script type="text/javascript">
        $(window).on('load',function(){
            setTimeout(function () {
                $('#myModal').modal('show');
            }, 3000);
        });
    </script>
    @endif
@endsection