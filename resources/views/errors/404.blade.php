@extends('layouts.master')

@section('content')
<div class="tittle-sub-top pad-top-150">
    <div class="container">
        <h1>
            404
        </h1>
    </div>
</div>
</header>
<div class="orange-arrow">
    &nbsp;
</div>
</section>
<!-- End Header Cake -->
<!-- Start 404 Page -->
<section class="404">
    <div class="container">
        <div class="content-404">
            <ul>
                <li>
                    4
                </li>
                <li>
                    <img alt="404" src="{{ asset('images/404.png') }}">
                </li>
                <li>
                    4
                </li>
            </ul>
            <div class="page-header text-center">
                <h3 class="text-center">
                    Oops! Pagina nu a fost gasita
                </h3>
                <form action="index.php">
                    <button class="btn btn-orange-cake mar-right-10" type="submit">Inapoi</button>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- End 404 Page -->
<div class="pad-top-150"></div>
<!-- Start Footer Cake -->
@endsection