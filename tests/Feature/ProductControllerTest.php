<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Category;
use App\Product;

class ProductControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_view_all_products_for_one_category()
    {
        $firstCategory = factory(Category::class)->create(['slug' => 'cakes']);
        $secondCategory = factory(Category::class)->create(['slug' => 'cookies']);

        $cake = factory(Product::class)->create(['category_id' => $firstCategory->id]);
        $cookie = factory(Product::class)->create(['category_id' => $secondCategory->id]);

        $this->get('/' . $firstCategory->slug)
            ->assertSee($cake->name)
            ->assertDontSee($cookie->name);
    }

    /** @test */
    public function it_can_view_a_single_product()
    {
        $firstCategory = factory(Category::class)->create(['slug' => 'cakes']);

        $cake = factory(Product::class)->create(['category_id' => $firstCategory->id]);
        $cookie = factory(Product::class)->create(['category_id' => 100]);

        $this->get('/' . $firstCategory->slug . '/' . $cake->id)
            ->assertSee($cake->name)
            ->assertDontSee($cookie->name);
    }
}
