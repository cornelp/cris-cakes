<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class CheckoutControllerTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    /** @test */
    public function it_can_save_a_new_order()
    {
        $data = [
            'product' => 1, 'structure' => 1,
            'serving' => 1, 'quantity' => 1
        ];

        $this->json('POST', '/cart', $data);

        $data = [
            'name' => 'Name',
            'email' => 'test@test.com',
            'phone' => '1234567890',
            'accept' => 1,
        ];

        $this->json('POST', '/checkout', $data)
            ->dump()
            ->assertStatus(200)
            ->assertSee('Multumim');
    }
}
