<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('under-construction')
    ->group(function () {
        Route::get('/', 'HomeController@index');

        Route::get('/structure/{structure}', 'StructureController@show');
        Route::get('/topper/{topper}', 'TopperController@show');

        // rute pentru manipulare cos
        Route::get('/cart', 'CartController@show');
        Route::put('/cart', 'CartController@update');
        Route::post('/cart', 'CartController@store');
        Route::delete('/cart', 'CartController@destroy');

        // continutul cosului
        Route::get('/basket', 'BasketController@index');

        Route::middleware('check-cart')
            ->group(function () {
                Route::get('/checkout', 'CheckoutController@index');
                Route::post('/checkout', 'CheckoutController@store')
                    ->middleware('check-invalid-date');
            });

        Route::get('/gallery', 'GalleryController@index');

        Route::get('/terms', 'TermsController@index');
        Route::get('/blog', 'BlogController@index');

        Route::prefix('admin')
            ->namespace('Admin')
            ->name('login')
            ->middleware('guest')
            ->group(function () {
                Route::get('/login', 'AdminController@show');
                Route::post('/login', 'AdminController@login');
            });

        Route::prefix('admin')
            ->namespace('Admin')
            ->middleware('auth')
            ->group(function () {
                Route::get('/', 'AdminController@index');
                Route::get('/logout', 'AdminController@logout');

                Route::resource('/gallery', 'GalleryController')
                    ->only(['index', 'store', 'destroy']);

                Route::resource('/products', 'ProductController')
                    ->except(['show', 'destroy']);

                Route::get('/products/{productId}/images', 'ProductImageController@index');
                Route::post('/products/{productId}/images', 'ProductImageController@store');
                Route::delete('/products/{productId}/images/{imageId}', 'ProductImageController@destroy');

                Route::resource('/structures', 'StructureController')
                    ->except(['destroy', 'show']);

                Route::resource('/toppers', 'TopperController')
                    ->except(['destroy' , 'show']);

                Route::resource('/servings', 'ServingController')
                    ->except(['destroy' , 'show']);

                Route::resource('/config', 'ConfigController')
                    ->only(['index', 'edit', 'update']);

                Route::get('/orders-processed', 'OrderController@processed');
                Route::get('/orders-unprocessed', 'OrderController@unprocessed');

                Route::get('/orders/{order}/edit', 'OrderController@show');
                Route::put('/orders/{order}', 'OrderController@update');

                Route::get('/announce', 'AnnounceController@index');
                Route::put('/announce', 'AnnounceController@update');

                Route::get('/reports', 'ReportController@index');

                Route::resource('/categories', 'CategoryController')
                    ->except(['show', 'destroy']);

                Route::resource('/comments', 'OrderCommentController')
                    ->only(['store', 'destroy']);

                Route::get('/invalid-dates', 'InvalidDateController@index');
                Route::post('/invalid-dates', 'InvalidDateController@store');
                Route::get('/invalid-dates/{id}/delete', 'InvalidDateController@destroy');

                Route::get('/manual-orders/create', 'ManualOrderController@create');
                Route::post('/manual-orders', 'ManualOrderController@store');

                Route::get('/orders/{order}/add-products/create', 'OrderProductController@create');
                Route::post('/orders/{order}/add-products', 'OrderProductController@store');
                Route::delete('/orders/{order}/details/{orderDetail}', 'OrderProductController@destroy');

                Route::get('orders/{order}/export', 'OrderExportController@excel');
            });

        // afisare produse si produse in categorii
        Route::middleware('check-category')
            ->group(function () {
                Route::get('/{categorySlug}', 'ProductController@index');
                Route::get('/{categorySlug}/{productId}', 'ProductController@show');
            });
    });
