<?php

return [

    /*
     * Activate under construction mode.
     */
    'enabled' => env('UNDER_CONSTRUCTION_ENABLED', true),

    /*
     * Hash for the current pin code
     */
    'hash' => env('UNDER_CONSTRUCTION_HASH', null),

    /*
     * Under construction title.
     */
    'title' => 'Site in lucru - Comenzile se pot da pe Facebook Atelier CrisCakes',

    /*
     * Back button translation.
     */
    'back-button' => 'inapoi',

    /*
    * Show button translation.
    */
    'show-button' => 'arata',

    /*
     * Hide button translation.
     */
    'hide-button' => 'ascunde',

    /*
     * Redirect url after a successful login.
     */
    'redirect-url' => '/',

    /*
     * Enable throttle (max login attempts).
     */
    'throttle' => true,

        /*
        |--------------------------------------------------------------------------
        | Throttle settings (only when throttle is true)
        |--------------------------------------------------------------------------
        |

        */
        /*
         * Set the maximum number of attempts to allow.
         */
        'max_attempts' => 3,

        /*
         * Show attempts left.
         */
        'show_attempts_left' => true,

        /*
         * Attempts left message.
         */
        'attempts_message' => 'Incercari ramase: %i',

        /*
         * Too many attempts message.
         */
        'seconds_message' => 'Prea multe incercari - vei putea reincerca in %i secunde.',

        /*
         * Set the number of minutes to disable login.
         */
        'decay_minutes' => 5,

        /*
         * Prevent the site from being indexed by Robots when locked
         */
        'lock_robots' => true,
];
