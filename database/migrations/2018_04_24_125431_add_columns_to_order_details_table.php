<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_details', function ($table) {
            $table->integer('serving_id')->unsigned()->nullable();
            $table->integer('topper_id')->unsigned()->nullable();
            $table->integer('structure_id')->unsigned()->nullable();
            $table->string('color')->nullable();
            $table->integer('quantity')->unsigned()->nullable();
            $table->integer('price')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_details', function ($table) {
            $table->dropColumn('serving_id');
            $table->dropColumn('topper_id');
            $table->dropColumn('structure_id');
            $table->dropColumn('color');
            $table->dropColumn('quantity');
            $table->dropColumn('price');
        });
    }
}
