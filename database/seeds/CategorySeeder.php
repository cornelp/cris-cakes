<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect(['cakes', 'cookies', 'dishes'])
            ->each(function ($item) {
                factory(Category::class)->create(['name' => ucfirst($item), 'slug' => $item]);
            });
    }
}
