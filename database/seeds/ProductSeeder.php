<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Category;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::get(['id'])->each(function ($item) {
            $product = factory(Product::class)->create(['name' => 'Tort ' . $item->id, 'category_id' => $item->id]);

            $product->images()->attach(1);
        });
    }
}
