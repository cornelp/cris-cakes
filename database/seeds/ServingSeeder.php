<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Serving;

class ServingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::get(['id'])->each(function ($item) {
            $serving = Serving::create([
                'name' => 'Portie ' . $item->id,
                'price' => rand(1000, 10000)
            ]);

            $item->servings()->attach($serving->id);
        });
    }
}
