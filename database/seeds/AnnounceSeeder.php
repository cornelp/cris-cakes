<?php

use Illuminate\Database\Seeder;
use App\Announce;

class AnnounceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Announce::create(['text' => '', 'active' => 0]);
    }
}
