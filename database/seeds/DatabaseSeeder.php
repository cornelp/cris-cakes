<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ImageSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(StructureSeeder::class);
        $this->call(TopperSeeder::class);
        $this->call(ServingSeeder::class);
        $this->call(AnnounceSeeder::class);
    }
}
