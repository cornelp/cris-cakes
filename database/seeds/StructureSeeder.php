<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Structure;

class StructureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::get(['id'])->each(function ($item) {
            $strcuture = Structure::create([
                'name' => 'Structura pentru ' . $item->id,
                'content' => 'Continut pentru ' . $item->id,
                'image' => 1
            ]);

            $item->structures()->attach($strcuture->id);
        });
    }
}
