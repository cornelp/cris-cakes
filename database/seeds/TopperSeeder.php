<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Topper;

class TopperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::get(['id'])->each(function ($item) {
            $topper = Topper::create([
                'name' => 'Topper ' . $item->id,
                'content' => 'Continut ' . $item->id,
                'image' => 1,
                'price' => rand(100, 10000)
            ]);

            $item->toppers()->attach($topper->id);
        });
    }
}
