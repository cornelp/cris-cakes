var addToCart = function (form) {
    removeErrors();
    showErrors(false);

    axios.post('/cart', form.serialize())
        .then(function (response) {
            updateCart(response.data.items);

            notifyUser('notice', 'Adaugat in cos.');
        })
        .catch(function (err) {
            notifyUser('error', 'Ai ceva erori');

            $('html,body').animate({ scrollTop: 0 }, 'slow');

            addToErrorsList(err.response.data.errors);

            showErrors(true);
        });
};

var removeErrors = function () {
    $('.alert-danger ul').html('');
};

var showErrors = function (show) {
    show
        ? $('.alert-danger').removeClass('hidden')
        : $('.alert-danger').addClass('hidden');
};

var addToErrorsList = function (errors) {
    Object.keys(errors).forEach(function (key) {
        errors[key].forEach(function (error) {
            $('.alert-danger ul').append('<li>' + error + '</li>');
        });
    });
}

var notifyUser = function (type, message) {
    var titles = { error: 'Atentie', notice: 'OK' };

    $.growl[type]({ message: message, title: titles[type] });
};

var updateCart = function (items) {
    count = Object.keys(items).length;
    
    if (count == 0) {
        return $('.notification-container').addClass('hidden');
    }

    $('.notification-container').removeClass('hidden');
    $('.notification-bubble').html(count);
};